<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'filin' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '1' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Azo[:kET&=`I,x8$_[S9Yv<,ug,z,ra|;BK(+Hz[Fr=`rnb=UT3G5<[OS#l&MwWD' );
define( 'SECURE_AUTH_KEY',  'GwJnHS*G,RQV`~J<dVW01YM*5=idNJCalpm3|bRs#xF^e>J?Vy$sAPJtv)o.<vxJ' );
define( 'LOGGED_IN_KEY',    '$QI@LTZK{MA7UgPqA2y(Yve7T&c.sX)b**X_vt9oy4eScW,U14H=6uDxv0HwhJ~ ' );
define( 'NONCE_KEY',        'C]M[HMs)_(^m8ILpNo)R1A+}fp_A`8L1*}H-wHz]9%7Op_@N|dwO7>;A7^ I:34;' );
define( 'AUTH_SALT',        'Z0v^C35GZSG$^%B?bNWBZ %p5#]$pgY~DrZk|lba$K9/V9[#S_9(*h -N#cm)`hE' );
define( 'SECURE_AUTH_SALT', '?GA%yDX ?B~bCS CgFBs1;UO_<SS yln>YCG7L4 PTdQTJ/2Ty4_AUm)*8mILJ5?' );
define( 'LOGGED_IN_SALT',   '52u|~wUXS~xDnT=gnv9@B}ZWMDMV0!~<mXYqU39j%D,<n5-rDK/N%LU:@^)h[~I?' );
define( 'NONCE_SALT',       'Ag{_eOtkQ0|WAZ H6WX3@Dsf]m!<_PF&0r7tOd4Dpy ^hfeQSk~6%]84FT%][h6J' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
