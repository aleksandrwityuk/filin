<?php
//Loadmore in main loop
function loadMore()
{
    $breadcrumbs = (int)$_POST['breadcrumbs'];
    $count = false;
    $num = 9;
    if ( isset( $_POST['category_id'] ) ) {
        $args = array(
            'cat' => $_POST['category_id'],
            'post_type' => 'post'
        );
        $the_query = new WP_Query( $args );
        wp_reset_postdata();
        $posts = get_posts(array(
            'numberposts' => $num,
            'cat'    => $_POST['category_id'],
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => 'post',
            'suppress_filters' => true,
            'offset'   => $_POST['offset']
        ));
    } elseif ( isset( $_POST['autor_id'] ) ) {
        $args = array(
            'author' => $_POST['autor_id'],
            'post_type' => 'post'
        );
        $the_query = new WP_Query( $args );
        wp_reset_postdata();
        $posts = get_posts(array(
            'numberposts' => $num,
            'author'    => $_POST['autor_id'],
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => 'post',
            'suppress_filters' => true,
            'offset'   => $_POST['offset']
        ));
    } elseif ( isset( $_POST['post_type'] ) ) {

        $post_type = $_POST['post_type'];
        $args = array(
            'post_type' =>  $post_type
        );
        $the_query = new WP_Query( $args );
        wp_reset_postdata();
        $posts = get_posts(array(
            'numberposts' => $num,
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => $post_type,
            'suppress_filters' => true,
            'offset'   => $_POST['offset']
        ));
    } else {
        $posts = get_posts(array(
            'numberposts' => $num,
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => 'post',
            'suppress_filters' => true,
            'offset'   => $_POST['offset']
        ));
    }

    foreach( $posts as $post ){
        $content .= loop_post($post, 'loading', $breadcrumbs);
    }
    wp_reset_postdata();

    if ($_POST['offset'] == ($the_query->found_posts - 2)) $count = true;

    $result = ['result' => $content, 'count' => $count];
    wp_send_json($result);
}
add_action('wp_ajax_loadMore', 'loadMore');
add_action('wp_ajax_nopriv_loadMore', 'loadMore');

//Add like
function addLike()
{
    $resp = '';
    $post_id =  $_POST['post_id'];
    $status = $_POST['status'];
    $post_id = intval($post_id);

    switch ($status) {
    case 'like':
        $likes = get_field( 'likes', $post_id );
        $likes++;
        update_field('likes', $likes, $post_id);
        $resp = $likes;
        break;
    case 'dislike':
        $dislikes = get_field( 'dislikes', $post_id );
        $dislikes++;
        update_field('dislikes', $dislikes, $post_id);
        $resp = $dislikes;
        break;
    }

    $result = ['result' => $resp];
    wp_send_json($result);
}
add_action('wp_ajax_addLike', 'addLike');
add_action('wp_ajax_nopriv_addLike', 'addLike');

// Оцени статью
function rateTheArticle()
{
    $post_id =  $_POST['post_id'];
    $status = $_POST['status'];
    $middle_ball = get_field( "middle_ball", $post_id );
    $total_voted = get_field( "total_voted", $post_id );
    if ( $total_voted == 0 ) {
        $res = $middle_ball + intval($status);
    } else $res = round( (($middle_ball + intval($status))/2), 1 );
    update_field('middle_ball', $res, $post_id); 
    $total = intval($total_voted) + 1;
    update_field('total_voted', $total, $post_id);

    $result = ['result' => $res, 'total' => $total];
    wp_send_json($result);
}
add_action('wp_ajax_rateTheArticle', 'rateTheArticle');
add_action('wp_ajax_nopriv_rateTheArticle', 'rateTheArticle');

/*--------------------------------------------*/
/* Search */
/*--------------------------------------------*/
function Search(){
    $sch = $_POST['search'];
    $args = [
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'suppress_filters' => true,
        'post_status' => 'publish'
    ];
    $query_posts = new WP_Query($args);
    $result = '<div class="row result-items"><ul>';
    $i = 0;
    $m = array();
    $hidden = '';
    while ( $query_posts->have_posts() ){ $query_posts->the_post();
        $post_id = get_the_ID();
        $title = get_the_title();
        $description = get_field('description', $post_id); 
        $pos = mb_stripos($title, $sch);
        $_pos = mb_stripos($description, $sch);
        $__pos = if_post_content_has_sch($post_id, $sch);
        $article_source = '';
        if ($pos !== false || $_pos !== false || $__pos == true) {
            $date = get_the_date( 'j F Y', $post_id ); 
            $author = get_autor_data($post_id);
            if ($author) {
                $article_source = $author['first_name'] . ' ' . $author['last_name'] . ', ' . $author['role']; 
            } else {
                $article_source = get_field('source', $post_id)['author'];
            }
            if ($i > 20) {
                $hidden = 'class="hidden"';
            }
            $result .= '<li '.$hidden.'>
                <a href="'. get_post_permalink($post_id) .'"><p>'.$title.'</p></a>
                    <div class="col-12">
                    <span class="article-date">' .$date.'</span>
                    <span class="article-info">'.$article_source.'</span>
                    </div>
            </li>';
            $i++;
        }
    }
    if ($i > 0) {
        $result .= '</ul></div><div class="button-block-center">
                <div class="btn-outlined btn-bright">
                    <svg class="icon">
                        <use xlink:href="'. get_template_directory_uri() .'/img/icons/svgmap.svg#reload" />
                    </svg>
                    <span class="text">
                        Загрузить еще
                    </span>
                </div>
            </div></div></section>';
        $result = '<section class="search-found"><div class="container"><div class="row title"><div class="col-12"><p>Вы искали: "'.$sch.'", найдено:'.$i.'</p></div>
        <div class="col-12"><p>Найденые странмцы:</p></div></div>' . $result;
    } else {
        $result = '<div class="row result-items"><div class="col-12">
        <p><h2>Ничего не найдено, уточните критерий поиска.</h2></p>
        <p>Обратите внимание на простые правила использования конкретного поиска по информации сайта:</p>
        <ul>
        <li>- Фраза для поиска может включать несколько слов. В результате будет произведен поиск содержимого, в котором имеются все эти слова.</li>
        <li>- Слова фразы могут быть неполными. Так, в частности, при поиске слова "крас" будут найдены и статьи, содержащие "красоты" и "прекрасно".</li>
        <li>- Регистр не оказывает влияния на результат поиска, т.е. "минск" и "Минск" дадут одинаковый результат.</li>
        </ul></div></div></section>';
        $result = '<section class="search-found"><div class="container"><div class="row title"><div class="col-12"><p>Вы искали: "'.$sch.'", найдено:'.$i.'</p></div>
        </div>' . $result;
    }
    

    //wp_send_json($result);
    echo $result;
    wp_die();
}
add_action('wp_ajax_Search', 'Search');
add_action('wp_ajax_nopriv_Search', 'Search');

function if_post_content_has_sch($post_id, $sch){
    $pos = false;
    if( have_rows('constructor',$post_id) ):
        // Loop through rows.
        while ( have_rows('constructor', $post_id) ) : the_row();
            // Case: Paragraph layout.
            switch (get_row_layout()) {
                case 'text_field':
                    $text = get_sub_field('text');
                    $pos = mb_stripos($text, $sch);
                    break;
                    if ($pos !== false) {
                        $pos = true;
                        return $pos;
                    }
            }
        // End loop.
        endwhile;
    endif;
    return $pos;
}