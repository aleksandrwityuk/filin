<?php



function theme_styles_and_scripts()
{
    //style
    wp_register_style('style', get_template_directory_uri() . '/dist/css/style.css', array(), '1.0', 'all');
    wp_enqueue_style('style');

    //scripts
    wp_register_script('script', get_template_directory_uri() . '/dist/js/script.js', array(), '1.0.0', true);
    wp_enqueue_script('script');

    //register basic value
    wp_localize_script('script', 'api_settings', array(
        'root'          => esc_url_raw(rest_url()),
        'nonce'         => wp_create_nonce('wp_rest'),
        'ajax_url'      => site_url() . '/wp-admin/admin-ajax.php',
        'template'      => get_bloginfo('template_url') . '/'
    ));

    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' );
}
