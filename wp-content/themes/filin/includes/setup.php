<?php

function setup_theme()
{
    header("Content-Type: text/html; charset=utf-8");
    //add thumbnails
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('automatic-feed-links');

    //menus
    register_nav_menus(array(
        'top_menu' => __('Top menu', 'filin'),
        'footer_menu' => __('Footer menu', 'filin')
    ));

    if (function_exists('acf_add_options_page')) {
        acf_add_options_page(array(
            'page_title'    => 'Theme settings',
            'menu_title'    => 'Theme settings',
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));
    }

    //add_theme_support( 'post-formats', array( 'quote' ) );
}

function filin_remove_editor_from_post_type() {
    remove_post_type_support( 'post', 'editor' );
}