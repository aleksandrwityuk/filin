<?php
//here are custom functions
// pagination
function pagination($pages = '', $range = 3)
{
    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }

    if ($pages == '') {
        global $query;
        $pages = $query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        $arrows = '<div class="arrows">';
        echo '<div class="paginate"><ul>';

        if ($paged != 1) {
            $arrows .= '<a class="left-arrow" href="' . get_pagenum_link($paged - 1) . '"></a>';
        }
        if ($paged != $pages) {
            $arrows .= '<a class="right-arrow" href="' . get_pagenum_link($paged + 1) . '"></a>';
        }

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<li class=\"current\"><a href=\"" . get_pagenum_link($i) . "\">" . $i . "</span></li>" : "<li><a href='" . get_pagenum_link($i) . "' class=\"page-numbers\">" . $i . "</a></li>";
            }
        }
        echo '</ul>';
        echo $arrows . '</div></div>';
    }
}

//Add a custom link to the end of a specific menu that uses the wp_nav_menu()
add_filter('wp_nav_menu_items', 'add_admin_link', 10, 2);
function add_admin_link($items, $args){
    if( $args->theme_location == 'top_menu' ){
        $items .= '<li class="item-with-search">
						<form class="form-search" id="form-search">
							<input type="text" value="'.get_search_query().'" name="s" class="input-search" id="input-search" placeholder="Поиск">
							<button class="btn-icon btn-search" id="btn-search" type="button">
								<svg class="icon">
									<use xlink:href="'.get_template_directory_uri().'/img/icons/svgmap.svg#search" />
								</svg>
							</button>
						</form>
					</li>';
    }
    return $items;
}

//Template post loop
function loop_post($post, $class_anim, $breadcrumbs=false) {
    $post_id = $post->ID;
    $teaser = get_field('teaser', $post_id);
    $post_format = get_field('post_format', $post_id);
    $category = get_the_category( $post_id );
    $term_link = get_term_link($category[0]->term_id);
    
    $post_author_info = get_autor_data($post_id);
    $autor_firstname = '';
    $autor_lastname = '';
    $author_link = '';
    $author_image_url = '';
    $article_source = '';
    if ($post_author_info) {
        $autor_firstname = $post_author_info['first_name'];
        $autor_lastname = $post_author_info['last_name'];
        $author_link = $post_author_info['author_archive_link'];
        $author_image_url = wp_get_attachment_image_url( $post_author_info['author_image'] );
        $article_source = $autor_firstname.' '.$autor_lastname;
    } else {
        $article_source = get_field('source', $post_id)['author'];
        $author_link = get_field('source', $post_id)['link'];
    }

    $breadcrumbs = ($breadcrumbs)? 'style="display: none;"': '';
    //Wrap
    $content = '<div class="col-lg-4 col-md-6 col-12 '.$class_anim.'">';
    //Condition posts
    if (($post_format == 'quote'|| has_category( 'author_column', $post_id)) && $post_author_info) :
        $content .= '
            <div class="author-quote large">
                <div class="author-row">
                    <span class="author-image cover-container">
                        <a href="'. $author_link .'" class="panel-title-link">
                            <img src="'.$author_image_url.'" class="cover" alt="">
                        </a>
                    </span>
                    <span class="author-name">'
                        .$autor_firstname.'<br/>'.$autor_lastname.
                    '</span>
                </div>
                <div class="author-quote-text">
                    <a href="'.get_post_permalink($post_id).'" class="panel-title-link">
                    <span class="author-quote-text-main">'
                                .$teaser.
                    '</span></a>
                </div>
            </div>';
    elseif ($post_format == 'donate') :
        $content .= '
            <div class="panel panel-support">
                <div class="panel-image cover-container">'
                    .get_the_post_thumbnail($post_id, 'medium', array('class'=>'cover') ).
                    '<div class="image-btn-block">
                        <a href="'.get_post_permalink($post_id).'" class="btn-outlined">
                            <span class="text">
                                Поддержать проект
                            </span>
                        </a>
                    </div>
                </div>
                <h2 class="panel-title">'
                    . $teaser .
                '</h2>
            </div>';
    else :
        $content .= '
            <div class="panel">
                <a href="'.get_post_permalink($post_id).'" class="panel-image cover-container">'
                    .get_the_post_thumbnail($post_id, 'medium', array('class'=>'cover')).
                '</a><a href="' . $term_link . '">
                <span class="panel-category">'
                    .$category[0]->name.
                '</span></a>
                <h2 class="panel-title">
                    <a href="'.get_post_permalink($post_id).'" class="panel-title-link">'
                            . $teaser .
                        '<span class="panel-main-title"></span><span class="panel-description-text"></span>
                    </a>
                </h2>
                <span class="panel-info" '.$breadcrumbs.'>'
                    .get_the_date("j M", $post).', '.$article_source.
                '</span>
            </div>';
    endif;
    //Wrap
    $content .= '</div>';

    return $content;
}

//Allow only one tax for post
function admin_js() { ?>
    <script type="text/javascript">
    jQuery(document).ready( function () { 
        jQuery('form#post').find('.categorychecklist input').each(function() {
            var new_input = jQuery('<input type="radio" />'),
            attrLen = this.attributes.length;
            for (i = 0; i < attrLen; i++) {
                if (this.attributes[i].name != 'type') {
                    new_input.attr(this.attributes[i].name.toLowerCase(), this.attributes[i].value);
                }
            }
            jQuery(this).replaceWith(new_input);
        });
    });
</script>
<?php }
//add_action('admin_head', 'admin_js');

//Rss
function get_Rss() {
    $url = "https://gazetaby.com/rss/zen/"; 
    $rss = simplexml_load_file($url);
    return $rss->channel->item;
}

//Get author data
function get_autor_data($post_id) {
    $term = wp_get_post_terms($post_id, 'quote_author', array('fields' => 'all'))[0];
    $result = array();
    if ($term):
        //Get term author (is term in current case)
        $term_author_settings = get_field('setting_author', $term);
        //Author image check
        $term_author_settings['image'] ?  $author_image = $term_author_settings['image'] : $author_image = get_field('default_image','option');
        $full_name = explode(' ', $term->name );
        $result = [
            'first_name' => $full_name[0], 
            'last_name' => $full_name[1],
            'author_image' => $author_image,
            'author_archive_link' => get_term_link($term->term_id),
            'role' => $term_author_settings['author_role'] 
        ];
    else: $result = false;
    endif;

    return $result;
}

//Add new author's role
function activate_filin_theme() {
    add_role( 'reviewer', 'Обозреватель',
        [
            'read'         => true,  
            'edit_posts'   => true,  
            'upload_files' => true, 
        ]
    );
}
//Remove author's role
function deactivate_filin_theme() {
    remove_role( 'reviewer' );
}

function filinn_get_recent_posts($num , $offset=0 ){
    $result = wp_get_recent_posts( [
        'numberposts'      => $num,
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true,
        'offset'            => $offset
    ], OBJECT );
    return $result;
}