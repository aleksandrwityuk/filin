<?php

//register custom post_type and taxonomy

function register_post_types()
{
    register_post_type(
        'quote',
        array(
            'label'  => null,
            'labels' => array(
                'name'               => 'quote',
                'singular_name'      => 'quote',
                'add_new'            => 'Добавить',
                'add_new_item'       => 'Редактировать',
                'edit_item'          => 'Редактировать',
                'new_item'           => 'Добавить',
                'view_item'          => 'Просмотреть',
                'search_items'       => 'Поиск',
                'not_found'          => 'Не найдено',
                'not_found_in_trash' => 'Не найдено',
                'parent_item_colon'  => '',
                'menu_name'          => 'Цитаты'
            ),
            'has_archive'         => true,
            'public'              => true,
            'exclude_from_search' => false,
            'show_in_menu'        => true,
            'menu_position'       => null,
            'supports'            => array('title', 'editor', 'thumbnail'),
            'taxonomies'  => array( 'category' )
        )
    );
    register_taxonomy('quote_author', array('quote', 'post'), array(
        'label'                 => '',
        'labels'                => array(
            'name'              => 'Автор',
            'singular_name'     => 'quote_author',
            'search_items'      => 'Поиск',
            'all_items'         => 'Всё',
            'view_item '        => 'Просмотреть',
            'parent_item'       => 'Надкатегория',
            'parent_item_colon' => 'Надкатегория:',
            'edit_item'         => 'Редактировать',
            'update_item'       => 'Обновить',
            'add_new_item'      => 'Добавить',
            'new_item_name'     => 'Добавить',
            'menu_name'         => 'Авторы'
        ),
        'description'           => '',
        'public'                => true,
        'publicly_queryable'    => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => true,
        'show_in_rest'          => null,
        'rest_base'             => null,
        'hierarchical'          => true,
        'update_count_callback' => '',
        'rewrite'               => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => true,
        '_builtin'              => false,
        'show_in_quick_edit'    => null,
    ));

    add_post_type_support( 'page', 'excerpt');
}
