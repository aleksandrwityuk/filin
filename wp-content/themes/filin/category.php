<?php get_header(); ?>
<?php $category = get_queried_object(); ?>
<main class="main" id="srchresult">
    
    <section class="section-category">
        <div class="container">
            <h1 class="category-name">
                <?php echo $category->name; ?>
            </h1>
        </div>
    </section>

    <section class="section-panels">
        <div class="container">
            <div class="row">
            <?php
            if (have_posts()):
                $itr = 0;
                while ( have_posts() ): the_post();
                    $post =  get_post();
                    if ( $itr < 9 ):
                        if (has_category( 'author_column', get_the_ID())) {
                            echo loop_post($post, '');
                        } else {
                            get_template_part('partials/loop-panel-post');
                        }
                    endif;
                    $itr++;
                endwhile;
            endif;
            ?>
            </div>
        </div>
    </section>
    <?php
        get_template_part('partials/large-banner');
    ?>
    <section class="section-panels" id="loadmore">
        <div class="container">
            <div class="row">
            <?php
            if (have_posts()):
                $itr = 0;
                while ( have_posts() ): the_post();
                    $post = get_post();
                    if ( $itr > 8 && $itr < 18):
                        if (has_category( 'author_column', get_the_ID())) {
                            echo loop_post($post, '');
                        } else {
                            get_template_part('partials/loop-panel-post');
                        } 
                    endif;
                    $itr++;
                endwhile;
            endif;
            ?>
            </div>
            <div class="button-block-center">
                <button :disabled="disabled" class="btn-outlined btn-bright" v-if="show" id="btn-loadmore" @cat="<?= $category->term_id; ?>">
                    <svg class="icon">
                        <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#reload" />
                    </svg>
                    <span class="text">
                        Загрузить еще
                    </span>
                </button>
            </div>
        </div>
    </section>
    <section class="section-special-panels">
        <div class="container">
            <?php get_template_part('page_templates/home-page/section-special-panels', null, 
        array('location' => 'footer')) ?>
        </div>
    </section>
</main>
<?php get_footer() ?>