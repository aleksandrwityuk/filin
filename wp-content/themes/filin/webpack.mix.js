const mix = require('laravel-mix');
require('laravel-mix-polyfill');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/*mix.webpackConfig({ node: { fs: 'empty' }})*/
mix
	.options({
        processCssUrls: false,
    })
    .js('src/js/script.js', 'dist/js')
    .sass('src/scss/style.scss', 'dist/css')
    /*.polyfill({
      enabled: true,
      useBuiltIns: "usage",
      targets: {"firefox": "50", "ie": 11}
   });*/
    /*.browserSync({
        proxy: 'filin.loc',
        files: ["dist/css", "dist/js", "*.php", "inc"]
    });*/