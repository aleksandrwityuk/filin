<form class="form-search" method="get" id="form-search" action="'.esc_url( home_url( '/' ) ).'">
	<input type="text" value="'.get_search_query().'" name="s" class="input-search" id="input-search" placeholder="Поиск">
	<button class="btn-icon btn-search" id="btn-search" type="button">
		<svg class="icon">
			<use xlink:href="'.get_template_directory_uri().'/img/icons/svgmap.svg#search" />
		</svg>
	</button>
</form>