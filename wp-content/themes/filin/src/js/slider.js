import $ from 'jquery';
document.addEventListener('DOMContentLoaded', function(){
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        adaptiveHeight: true,
        asNavFor: '.slider-thumbnails'
    });
    $('.slider-thumbnails').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: false,
        arrows: true,
        variableWidth: true,
        focusOnSelect: true
    });
});