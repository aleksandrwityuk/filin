require('./wp_script.js');
import 'popper.js';
import 'bootstrap';
import './slider';
import "../../node_modules/slick-slider/slick/slick";

document.addEventListener('DOMContentLoaded', function(){
    var btnMenu = document.getElementById('btn-menu');
    var header = document.getElementById('header');

    if(btnMenu != undefined){
        btnMenu.addEventListener('click', function(){
            if (!header.classList.contains('open-menu')){
                header.classList.remove('open-search');
                header.classList.add('open-menu');
                document.body.classList.add('body-overflow');
            } else {
                header.classList.remove('open-menu');
                document.body.classList.remove('body-overflow');
            }
        });
    }

    var btnSearch = document.getElementById('btn-search');
    var formSearch = document.getElementById('form-search');
    var inputSearch = document.getElementById('input-search');
    var btnSearchMobile = document.getElementById('btn-search-mob');
    var loadMoreSearch;
    var searchElem;
    if(btnSearch != undefined){
        if(window.innerWidth > 640){
            btnSearch.addEventListener('click', function(){
                inputSearch.classList.add('visible');
                //console.log(inputSearch.classList.contains('visible'));
                let search_query = inputSearch.value;
                if (search_query.length > 0) {
                    search_ajax(search_query);
                }
            });
            document.addEventListener('click', function(e){
                var target = e.target;
                var thisSelectBlock = target == formSearch || formSearch.contains(target);
                if(!thisSelectBlock){
                    inputSearch.classList.remove('visible');
                }
            });
        } else {
            btnSearchMobile.addEventListener('click', function(){
                if(!header.classList.contains('open-search')){
                    header.classList.remove('open-menu');
                    header.classList.add('open-search');
                    document.body.classList.add('body-overflow');
                } else {
                    header.classList.remove('open-search');
                    document.body.classList.remove('body-overflow');
                }          
            });
        }
        
    }
    inputSearch.addEventListener("keyup", function(event) {
        var keyCode = (window.event) ? event.which : event.keyCode;
        if (keyCode && keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    
    function searchLoadMore(){
        var items = '';
        items = searchElem.getElementsByClassName("hidden");
        //console.log(items.length);
        var k = 20
        while(items.length) {
            if (k > 0) {
                 items[0].classList.remove('hidden');
                 k--;
            }
            else break;
            //console.log(k);
        }
    }

    function search_ajax(str) {
        //console.log('VALUE: '+ str);
        let handler = 'Search';
 
        let data = 'action='+ handler +'&search='+ str;
        
        let link = api_settings.ajax_url + '?action=' + handler;

        const request = new XMLHttpRequest();
         
        request.open("POST", link, true);
         
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
         
        request.onreadystatechange = function() {
            if( request.readyState == 4 && request.status === 200 ) {       
                //console.log(request.responseText);
                var html = request.responseText;
                searchElem = document.getElementById('srchresult');
                searchElem.innerHTML = html;
                loadMoreSearch = searchElem.getElementsByClassName('btn-outlined')[0];
                if(loadMoreSearch && loadMoreSearch != 'undefined'){
                    loadMoreSearch.addEventListener('click', searchLoadMore );
                }
            }
        }
        request.send(data);
    }

    var stars = document.querySelectorAll('.stars');
    if(stars.length > 0){
        for(let k = 0; k < stars.length; k++){
            var star = stars[k].querySelectorAll('.star');

            for(let i = 0; i < star.length; i++){
                star[i].addEventListener('click', function(){
                    stars[k].classList.add('mark-edded');
                });

                
                star[i].addEventListener('mouseenter', function(){
                    if(!stars[k].classList.contains('mark-edded')){
                        var currentIndex = i;
                        for(let j = 0; j < star.length; j++){
                            if(currentIndex >= j){
                                star[j].classList.add('red');
                            } else {
                                star[j].classList.remove('red');
                            }
                        }
                    }
                });

            }

            stars[k].addEventListener('mouseleave', function(){
                if(!stars[k].classList.contains('mark-edded')){
                    var currentStars = this.querySelectorAll('.star');
                    for(var t = 0; t < currentStars.length; t++){
                        currentStars[t].classList.remove('red');
                    }
                }
            });

        }
    }

    var priceInput = document.querySelectorAll('.price-input');
    var priceRadio = document.querySelectorAll('.price-radio');
    if(priceInput.length > 0){
        for(var i = 0; i < priceInput.length; i++){
            priceInput[i].addEventListener('input', function(){
                if(this.value.length > 0){
                    this.classList.add('checked');
                    var radioInput = this.parentElement.parentElement.querySelectorAll("input[type=radio]");
                    for(var j = 0; j < radioInput.length; j++){
                        radioInput[j].checked = false;
                    }
                } else {
                    this.classList.remove('checked');
                }
            });
        }

        for(var r = 0; r < priceRadio.length; r++){
            priceRadio[r].addEventListener('change', function(){
                var numberInput = this.parentElement.parentElement.querySelectorAll("input[type=number]");
                for(var t = 0; t < numberInput.length; t++){
                    numberInput[t].classList.remove('checked');
                    numberInput[t].value = '';
                }
            });
        }
    }

    var questionAccordeon = document.querySelectorAll('.question-accordeon');
    for(var i = 0; i < questionAccordeon.length; i++){
        var questionAsk = questionAccordeon[i].querySelector('.question-ask');
        questionAsk.addEventListener('click', function(){
            this.parentElement.classList.toggle('opened');
        });
    }
});
