//Polifills
import "core-js/modules/es.promise";
import "core-js/modules/es.array.iterator";

var btn_readmore = document.getElementById("btn-loadmore");
if(btn_readmore){
    btn_readmore.addEventListener("click", function() {
        //console.log('Yesss!!!');
        let handler = 'loadMore';
        let cat_id = this.getAttribute("@cat");
        let autor_id = this.getAttribute('@autor');
        let post_type = this.getAttribute('@post_type');
        let brdcrbs = this.getAttribute('@brdcrbs');
        //console.log(brdcrbs);

        let items = document.querySelectorAll('#loadmore .panel');

        items = items.length + 9;

        let data = 'action='+ handler +'&data=response';
        if (cat_id) {
            data = 'action='+ handler +'&data=response&category_id='+ cat_id+'&offset='+items+'&breadcrumbs='+brdcrbs;
        }
        else if (autor_id) {
            //console.log( 'autor_id:',autor_id);
            data = 'action='+ handler +'&data=response&autor_id='+ autor_id +'&offset='+items+'&breadcrumbs='+brdcrbs;
        }
        else if (post_type) { 
           //console.log(post_type);
            data = 'action='+ handler +'&data=response&post_type='+ post_type +'&offset='+items+'&breadcrumbs='+brdcrbs; 
        }
        
        let link = api_settings.ajax_url + '?action=' + handler;

        const request = new XMLHttpRequest();
         
        request.open("POST", link, true);
         
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
         
        request.onreadystatechange = function() {
            if( request.readyState == 4 && request.status === 200 ) {       
                //console.log(request.responseText);
                var obj = JSON.parse(request.responseText);
                var parser = new DOMParser();
                var html = parser.parseFromString(obj.result, 'text/html');
                for (var i = html.body.children.length - 1; i >= 0; i--) {
                    document.getElementById("loadmore").firstElementChild.firstElementChild.append(html.body.children[i]);
                }
                if (obj.count) {
                    let btn_loadmore = document.getElementById("btn-loadmore");
                    btn_loadmore.remove();
                }
            }
        }
        request.send(data);
        setTimeout(deleteClass, 2000);
    });
}




function deleteClass() {
    document.querySelectorAll('#loadmore .loading').forEach((el) => {
        el.classList.remove("loading")
    });
}

document.querySelectorAll('.btn-mark').forEach((el) => {
    el.addEventListener("click", function() {
        //console.log('btn_like_mark!');
 
        let post_id = el.getAttribute("@click");
        //console.log(post_id);
        var res_id = post_id.split("'");
        var name = 'filin_add_like' + res_id[1];
        
        if ( !getCookie(name) ) {
            var value = true;
            var props = {
                'path' : '/'
            };
            setCookie(name, value, props);
            addlikeDislike(res_id, el);
        } else console.log(document.cookie);
    });
});

function addlikeDislike(res_id, el) {
    let val = el.nextElementSibling;
    //console.log(val.innerHTML);
    let status = res_id[0].substring(0, res_id[0].length - 1);
    //console.log(status);
    let data = 'action=addLike&post_id='+res_id[1]+'&status='+status;
    //console.log(data);
    let handler = 'addLike';
    let link = api_settings.ajax_url + '?action=' + handler;

    const request = new XMLHttpRequest();
     
    request.open("POST", link, true);
     
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
     
    request.onreadystatechange = function() {
        if( request.readyState == 4 && request.status === 200 ) {       
            //console.log(request.responseText);
            var obj = JSON.parse(request.responseText);
            //console.log(obj.result.toString());
            val.innerHTML = obj.result.toString();
        }
    }
    request.send(data);
}

function getCookie(name) {

    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

function setCookie(name, value, props) {

    props = props || {}

    var exp = props.expires

    if (typeof exp == "number" && exp) {

        var d = new Date()

        d.setTime(d.getTime() + exp*1000)

        exp = props.expires = d

    }

    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

    value = encodeURIComponent(value)

    var updatedCookie = name + "=" + value

    for(var propName in props){

        updatedCookie += "; " + propName

        var propValue = props[propName]

        if(propValue !== true){ updatedCookie += "=" + propValue }
    }

    document.cookie = updatedCookie

}

// Star
document.querySelectorAll('.star').forEach((el) => {
    el.addEventListener("click", function() {
        var red_star = document.getElementsByClassName("red");
        //console.log('Yesss!!!');
        //console.log(red_star.length);
        var post_id = el.getAttribute("@click");
        var name = 'filin_add_raiting' + post_id;
        if ( !getCookie(name) ) {
            var value = true;
            var props = {
                'path' : '/'
            };
            setCookie(name, value, props);
            let handler = 'rateTheArticle';
            let data = 'action='+handler+'&post_id='+post_id+'&status='+ red_star.length;
            //console.log(data);
            let link = api_settings.ajax_url + '?action=' + handler;

            const request = new XMLHttpRequest();
             
            request.open("POST", link, true);
             
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
             
            request.onreadystatechange = function() {
                if( request.readyState == 4 && request.status === 200 ) {       
                    console.log(request.responseText);
                    let obj = JSON.parse(request.responseText);
                    let raiting = document.getElementsByClassName("article-raiting");
                    raiting[0].firstElementChild.firstElementChild.innerHTML = obj.result.toString();
                    raiting[0].getElementsByClassName("total")[0].innerHTML = '('+obj.total.toString()+')';
                }
            }
            request.send(data);
        } else console.log(document.cookie);
    });
})

//Main tools
/*
import Vue from "vue";
import axios from "axios";
import qs from 'qs';

const loadmore = new Vue({
    el: '#loadmore',
    data() {
        return {
            numberposts: 0,
            offset: 0,
            post_cat: 0,
            query_count: 0,
            show: true,
            disabled: false
        }
    },
    methods: {
        loadmore() {
            this.disabled = true;
            axios({
                method: 'post',
                url: window.api_settings.ajax_url + "?action=loadMore",
                data: qs.stringify({
                    offset: this.offset,
                    post_cat: this.post_cat
                })
            })
                .then(response => {
                    this.$refs.posts.innerHTML += response.data.result;
                    this.offset = this.offset + this.numberposts;
                    //hide button if no more posts
                    this.disabled = false;
                    
                    setTimeout(() => this.deleteClass(), 1);
                    
                    if (this.query_count <= this.offset) {
                        this.show = false;
                    }
                })
                .catch(error => {
                    this.disabled = false;
                });
        },
        deleteClass() {
            document.querySelectorAll('#loadmore .loading').forEach((el) => {
                el.classList.remove("loading")
            });
        }
    },
    created: function () {
        this.query_count = parseInt(window.load_more.query_count, 10);
        this.post_cat = parseInt(window.load_more.post_cat, 10);
        this.numberposts = parseInt(window.load_more.numberposts, 10);
        this.offset = this.numberposts;
    }
});

const likes = new Vue({
    el: '#likes',
    data() {
        return {
            post_id: 0
        }
    },
    methods: {
        like(post_id) {
            this.post_id = post_id;
            //alert('plus on ' + this.post_id);
            this.set('like');
        },
        dislike(post_id) {
            this.post_id = post_id;
            //alert('minus on ' + this.post_id);
            this.set('dislike');
        },
        set(status) {
            axios({
                method: 'post',
                url: window.api_settings.ajax_url + "?action=addLike",
                data: qs.stringify({
                    post_id: this.post_id,
                    status: status
                })
            })
                .then(response => {
                    alert(response.data.result);
                })
                .catch(error => {
                    alert(response.data.result);
                });
        }
    }
});
*/