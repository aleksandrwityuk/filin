<?php
    $post_id = get_the_ID();

    $author = get_autor_data($post_id);
    $terms = get_the_terms( $post_id, 'quote_author' );
    $term_link = get_term_link($terms[0]->term_id);
?>
<div class="author-quote large">
    <div class="author-row">
            <div class="author-image cover-container">
                <a href="<?= $term_link ?>">
                <?= wp_get_attachment_image($author['author_image'], 'thumbnail') ?>
                </a>
            </div>
        <span class="author-name">
            <?= $author['first_name']; ?><br/><?= $author['last_name']; ?>
        </span>
    </div>
    <div class="author-quote-text">
        <span class="author-quote-text-main">
            <?php the_content(); ?>
        </span>
        <div class="d-block d-sm-none button-block-center">
            <a href="<?= $term_link ?>" class="btn-outlined btn-bright">
                <span class="text">
                    Читать статьи
                </span>
            </a>
        </div>
    </div>
</div>