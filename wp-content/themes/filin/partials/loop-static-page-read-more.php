<?php
    $cat_name = $args['category_name'];
    $post_id = get_the_ID();
    $description = get_field('description', $post_id);

    $result = get_autor_data($post_id);
?>
<div class="panel">
    <span class="panel-category">
        <?php echo $cat_name; ?>
    </span>
    <h2 class="panel-title">
        <a href="<?php echo get_post_permalink() ?>" class="panel-title-link">
            <span class="panel-main-title"> <?php the_title(); ?></span>
            <span class="panel-description-text">
            <?= $description; ?>
            </span>
        </a>
    </h2>
    <span class="panel-info">
        <?= $result['first_name']; ?><br/><?= $result['last_name']; ?>
    </span>
</div>