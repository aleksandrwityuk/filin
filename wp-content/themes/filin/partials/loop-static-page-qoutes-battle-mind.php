<?php
    $post_id = get_the_ID();

    $result = get_autor_data($post_id);
?>
<div class="big-quote">
    <span class="big-quote-text">
        <?php the_title(); ?>
    </span>
    <div class="big-quote-row">
        <div class="big-quote-author">
            <div class="big-quote-author-image cover-container">
                <?= wp_get_attachment_image($result['author_image'], 'thumbnail') ?>
            </div>
            <span class="big-quote-author-name">
                <?= $result['first_name']; ?><br/><?= $result['last_name']; ?>
            </span>
        </div>
        <div class="quote-result">
            <div class="mark-row">
                <button @click="like('<?= $post_id ?>')" class="btn-mark" type="button">
                    <img src="<?= get_template_directory_uri() ?>/img/icons/plus.svg" alt="" class="icon">
                </button>
                <span class="mark-text"><?= get_field('likes') ?></span>
            </div>
            <div class="mark-row">
                <button @click="dislike('<?= $post_id ?>')" class="btn-mark" type="button">
                    <img src="<?= get_template_directory_uri() ?>/img/icons/minus.svg" alt="" class="icon">
                </button>
                <span class="mark-text"><?= get_field('dislikes') ?></span>
            </div>
        </div>
    </div>
</div>