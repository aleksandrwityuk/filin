<?php
    $post_id = get_the_ID();
    $teaser = get_field('teaser', $post_id);
    $post_author_info = get_autor_data($post_id);
    $autor_firstname = $post_author_info['first_name'];
    $autor_lastname = $post_author_info['last_name'];
    $author_image_url = wp_get_attachment_image_url( $post_author_info['author_image'] );
    $author_link = $post_author_info['author_archive_link'];
?>
<div class="author-row">
    <span class="author-image cover-container">
        <a href="<?= $author_link ?>" class="panel-title-link">
            <img src="<?= $author_image_url ?>" class="cover" alt="">
        </a>
    </span>
    <span class="author-name">
        <?= $autor_firstname ?><br/><?= $autor_lastname ?>
    </span>
</div>
<div class="author-quote-text">
    <a href="<?= get_post_permalink($post_id) ?>" class="panel-title-link">
    <span class="author-quote-text-main">
                <?= $teaser ?>
    </span></a>
</div>