<?php
    $post_id = get_the_ID();

    $result = get_autor_data($post_id);
?>
<div class="col-md-6 col-12">
    <div class="big-quote">
        <span class="big-quote-text">
            <?= get_the_content() ?>
        </span>
        <div class="big-quote-row">

            <!-- Display link if author is user -->
            <?= $result['author_is_user'] ? '<a href="'.$result['user_archive_link'] .'" class="big-quote-author">' : '<div class="big-quote-author">' ?>
                <div class="big-quote-author-image cover-container">
                    <?= wp_get_attachment_image($result['author_image'], 'thumbnail') ?>
                </div>
                <span class="big-quote-author-name">
                    <?= $result['first_name'] ?><br/><?= $result['last_name'] ?>
                </span>
            <?= $result['author_is_user'] ? '</a>' : '</div>' ?>
            
            <div class="quote-result">
                <div class="mark-row">
                    <button @click="like('<?= $post_id ?>')" class="btn-mark" type="button">
                        <img src="<?= get_template_directory_uri() ?>/img/icons/plus.svg" alt="" class="icon">
                    </button>
                    <span class="mark-text"><?= get_field('likes') ?></span>
                </div>
                <div class="mark-row">
                    <button @click="dislike('<?= $post_id ?>')" class="btn-mark" type="button">
                        <img src="<?= get_template_directory_uri() ?>/img/icons/minus.svg" alt="" class="icon">
                    </button>
                    <span class="mark-text"><?= get_field('dislikes') ?></span>
                </div>
            </div>

        </div>
    </div>
</div>