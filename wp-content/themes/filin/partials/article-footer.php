<?php
    $this_post_id = $args['post-id'];
?>
<div class="article-footer">
    <div class="social-buttons-amount">
    <?php
    reset_rows(); //  It is very important to use here!!!
    if( have_rows('social_share', 'option') ): 
        while( have_rows('social_share', 'option') ) : the_row(); 
            if ( get_sub_field('link')['title'] == 'rss' || get_sub_field('link')['title'] == 'telegram'){

            } else {
                ?>
                <a target="<?= get_sub_field('link')['target'] ?>" href="<?= get_sub_field('link')['url'] ?>" class="btn-social">
                <svg class="icon">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#<?= get_sub_field('image') ?>" />
                </svg>
                </a>
                <?php
            };
        endwhile;
    endif;
    ?>
    </div>
    <div class="article-mark">
        <span class="text">Оцени статью</span>
        <div class="stars">
            <button type="button" class="star" @click="<?= $this_post_id; ?>">
                <svg class="icon">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#star" />
                </svg>
            </button>
            <button type="button" class="star" @click="<?= $this_post_id; ?>">
                <svg class="icon">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#star" />
                </svg>
            </button>
            <button type="button" class="star" @click="<?= $this_post_id; ?>">
                <svg class="icon">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#star" />
                </svg>
            </button>
            <button type="button" class="star" @click="<?= $this_post_id; ?>">
                <svg class="icon">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#star" />
                </svg>
            </button>
            <button type="button" class="star" @click="<?= $this_post_id; ?>">
                <svg class="icon">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#star" />
                </svg>
            </button>
        </div>
    </div>
    <div class="article-raiting">
        <span class="text">
            Средний балл 
            <span class="mark">
            <?php 
            echo get_field( "middle_ball", $this_post_id ); 
            ?> 
            </span> 
        </span>
        <span class="total">
            <?php 
            echo '('. get_field( "total_voted", $this_post_id ) . ')'; 
            ?>
        </span>
    </div>
</div>