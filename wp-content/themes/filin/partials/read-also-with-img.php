<?php
    $cat_name = $args['category_name'];
    $post_id = get_the_ID();
    $post_description = get_field( "description", $post_id );

    $author = get_autor_data($post_id);
?>
<div class="border-block">
    <span class="border-name-block">
        <span class="border-name">
            Читайте также
        </span>
    </span>
    <div class="border-panel">
        <a href="<?php echo get_post_permalink() ?>" class="panel-image cover-container" style="padding-top: 0;">
            <?php the_post_thumbnail(); ?>
        </a>
        <div class="panel">
            <span class="panel-category">
                <?php echo $cat_name; ?>
            </span>
            <h2 class="panel-title">
                <a href="<?php echo get_post_permalink() ?>" class="panel-title-link">
                    <span class="panel-main-title"><?php the_title(); ?></span>
                    <span class="panel-description-text">
                        <?= $post_description; ?>
                    </span>
                </a>
            </h2>
            <span class="panel-info">
                <?= $author['first_name'] ?><br/><?= $author['last_name'] ?>
            </span>
        </div>
    </div>
</div>