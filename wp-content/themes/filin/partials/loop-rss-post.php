<a href="<?= $rss_post['link'] ?>" class="panel-image cover-container">
    <picture class="panel-picture cover">
        <img src="<?= $rss_post['image_url'] ?>" alt="<?= $rss_post['title'] ?>" class="cover">
    </picture>
</a>
<h2 class="panel-newspaper-title">
    <a href="<?= $rss_post['link'] ?>" class="panel-newspaper-title-link">
        <span class="panel-newspaper-title-main">
            <?= $rss_post['title'] ?>
        </span>
        <span class="panel-newspaper-title-description">
            <?= $rss_post['content'] ?>
        </span>
    </a>
</h2>