<?php
    $post_id = get_the_ID();
    $teaser = get_field('teaser', $post_id);
    $post_description = get_field( "description", $post_id );
    $category = wp_get_post_categories( $post_id, array('fields' => 'all') );   
    $date = get_the_date( 'j M', $post_id );
    $post_author_info = get_autor_data($post_id);
    $autor_firstname = '';
    $autor_lastname = '';
    $author_link = '';
    $author_image_url = '';
    $article_source = '';
    if ($post_author_info) {
        $autor_firstname = $post_author_info['first_name'];
        $autor_lastname = $post_author_info['last_name'];
        $author_link = $post_author_info['author_archive_link'];
        $author_image_url = wp_get_attachment_image_url( $post_author_info['author_image'] );
        $article_source = $autor_firstname.' '.$autor_lastname;
    } else {
        $article_source = get_field('source', $post_id)['author'];
        $author_link = get_field('source', $post_id)['link'];
    }
?>
<div class="col-lg-4 col-md-6 col-12">
    <div class="panel">
        <a href="<?= get_post_permalink($post_id) ?>" class="panel-image cover-container">
            <picture class="panel-picture cover">
                <?php the_post_thumbnail(); ?>
            </picture>
        </a>
        <!--<span class="panel-category">
            <?php //$category[0]->name; ?>
        </span>-->
        <h2 class="panel-title">
            <a href="<?= get_post_permalink($post_id) ?>" class="panel-title-link">
                <span class="panel-main-title"><?= $teaser ?></span><span class="panel-description-text">
                </span>
            </a>
        </h2>
        <span class="panel-info">
            <?= $date . ', ' . $article_source ?>
        </span>
    </div>
</div>