<?php get_header(); ?>
<?php
    $author_image_url = wp_get_attachment_image_url( $author['author_image'] );
    $author_link = $author['author_archive_link'];
    $role = $author['role'];
?>
<main class="main">

    <section class="section-author section-author-top">
        <div class="container">
            <div class="author-quote author-block large">
                <div class="author-row">
                    <span class="author-image cover-container">
                        <img src="<?= $author_image_url ?>" class="cover" alt="">
                    </span>
                    <div class="author-column">
                        <span class="author-name">
                            <?= $author['first_name'] ?> <br/> <?= $author['last_name']  ?>
                        </span>
                        <span class="author-desc">
                            <?= $role ?>
                        </span>
                    </div>
                </div>
                <div class="author-quote-text">
                    <h1 class="author-quote-text-title">
                        Поддержите независимую журналистику в Беларуси.
                    </h1>
                    <span class="author-quote-text-main">
                        Здесь вы можете сделать разовый платеж или оформить подписку.
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section class="section-blocks">
        <div class="container">
            <div class="border-block">
                <span class="border-name-block">
                    <span class="border-name">
                        Поддержать проект
                    </span>
                </span>
                <form class="border-block-center">
                    <div class="border-block-wrap">
                        <div class="type-labels-amount">
                            <label class="type-label">
                                <input type="radio" name="donut-type" checked>
                                <span class="label-text">Банковская карта</span>
                            </label>
                            <label class="type-label">
                                <input type="radio" name="donut-type">
                                <span class="label-text">Расчет ЕРИП</span>
                            </label>
                            <label class="type-label">
                                <input type="radio" name="donut-type">
                                <span class="label-text">SMS-сообщение</span>
                            </label>
                            <label class="type-label">
                                <input type="radio" name="donut-type">
                                <span class="label-text">Юридическим лицам</span>
                            </label>
                        </div>
                        <div class="double-labels-amount">
                            <label class="double-label">
                                <input type="radio" name="count-type" checked>
                                <span class="label-text">Ежемесячно</span>
                            </label>
                            <label class="double-label">
                                <input type="radio" name="count-type">
                                <span class="label-text">Разово</span>
                            </label>
                        </div>
                        <div class="round-labels-amount">
                            <label class="round-label">
                                <input type="radio" name="price-type" class="price-radio">
                                <span class="label-text">3 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="radio" name="price-type">
                                <span class="label-text">5 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="radio" name="price-type" class="price-radio" checked>
                                <span class="label-text">10 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="radio" name="price-type" class="price-radio">
                                <span class="label-text">20 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="radio" name="price-type" class="price-radio">
                                <span class="label-text">50 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="radio" name="price-type" class="price-radio">
                                <span class="label-text">100 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="radio" name="price-type" class="price-radio">
                                <span class="label-text">200 руб.</span>
                            </label>
                            <label class="round-label">
                                <input type="number" class="price-input" placeholder="Другая сумма">
                            </label>
                        </div>
                    </div>
                    <span class="dashed-separator"></span>
                    <div class="border-block-wrap">
                        <div class="mail-row">
                            <input type="email" class="mail-input" placeholder="Ваша электронная почта">
                            <button type="submit" class="btn-outlined btn-colored">
                                Поддержать
                            </button>
                        </div>
                    </div>
                    <div class="border-block-wrap">
                        <div class="social-row">
                            <span class="text-sm">
                                Нас можно поддержать, подписавшись на наши соцсети:
                            </span>
                            <ul class="social-list">
                                <li>
                                    <a href="/">
                                        <svg class="icon">
                                            <use xlink:href="/img/icons/svgmap.svg#facebook" />
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="/">
                                        <svg class="icon">
                                            <use xlink:href="/img/icons/svgmap.svg#vk" />
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="/">
                                        <svg class="icon">
                                            <use xlink:href="/img/icons/svgmap.svg#twitter" />
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="/">
                                        <svg class="icon">
                                            <use xlink:href="/img/icons/svgmap.svg#telegram" />
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="/">
                                        <svg class="icon">
                                            <use xlink:href="/img/icons/svgmap.svg#rss" />
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section class="section-article-more">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-12">
                    <div class="t-center">
                        <h2 class="article-more-title">
                            Почему это важно?
                        </h2>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="article-more-text">
                        <p>
                            Друзья! На прочтение этого текста вам понадобится примерно 3 минуты 40 секунд. Мы знаем, как ценно ваше время, и поэтому просим вас потратить 3 минуты 40 секунд своей жизни на этот текст. Прочитав его, вы навсегда измените судьбу проекта «Филин».
                        </p>
                    </div>
                    <a href="#" class="btn-article-more">
                        <span class="round">
                            <img src="/img/icons/eye.svg" alt="" class="icon">
                        </span>
                        <span class="text">Читать</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="section-questions">
        <div class="container">
            <div class="border-block">
                <span class="border-name-block">
                    <span class="border-name">
                        Часто задаваемые вопросы
                    </span>
                </span>
                <div class="questions-wrap">
                    <div class="question-accordeon">
                        <div class="question-ask">
                            <span class="ask-sign"></span>
                            <p>
                                Я не могу сделать большое пожертвование. Помогут ли вам мои 100 рублей?
                            </p>
                        </div>
                        <div class="question-answer">
                            <p>
                                Каждый месяц сайт «Филин» посещают порядка 6 млн человек. Если каждый посетитель будет ежемесячно перечислять нам всего по 10 рублей, мы сможем запланировать еще больше командировок, выпустить новые спецпроекты и провести еще больше расследований, за которые нас все ценят.
                            </p>
                            <p>
                                Представьте, сколько всего можно будет сделать благодаря ежемесячным пожертвованиям в 10 рублей!
                            </p>
                            <p>
                                И кстати, 10 рублей — самая частая сумма пожертвования.
                            </p>
                        </div>
                    </div>
                    <div class="question-accordeon">
                        <div class="question-ask">
                            <span class="ask-sign"></span>
                            <p>
                                Кто может стать соучастником?
                            </p>
                        </div>
                        <div class="question-answer">
                            <p>
                                Каждый месяц сайт «Филин» посещают порядка 6 млн человек. Если каждый посетитель будет ежемесячно перечислять нам всего по 10 рублей, мы сможем запланировать еще больше командировок, выпустить новые спецпроекты и провести еще больше расследований, за которые нас все ценят.
                            </p>
                            <p>
                                Представьте, сколько всего можно будет сделать благодаря ежемесячным пожертвованиям в 10 рублей!
                            </p>
                            <p>
                                И кстати, 10 рублей — самая частая сумма пожертвования.
                            </p>
                        </div>
                    </div>
                    <div class="question-accordeon">
                        <div class="question-ask">
                            <span class="ask-sign"></span>
                            <p>
                                Вы просите данные банковской карты. Это безопасно?
                            </p>
                        </div>
                        <div class="question-answer">
                            <p>
                                Каждый месяц сайт «Филин» посещают порядка 6 млн человек. Если каждый посетитель будет ежемесячно перечислять нам всего по 10 рублей, мы сможем запланировать еще больше командировок, выпустить новые спецпроекты и провести еще больше расследований, за которые нас все ценят.
                            </p>
                            <p>
                                Представьте, сколько всего можно будет сделать благодаря ежемесячным пожертвованиям в 10 рублей!
                            </p>
                            <p>
                                И кстати, 10 рублей — самая частая сумма пожертвования.
                            </p>
                        </div>
                    </div>
                    <div class="question-accordeon">
                        <div class="question-ask">
                            <span class="ask-sign"></span>
                            <p>
                                Почему важны ежемесячные пожертвования?
                            </p>
                        </div>
                        <div class="question-answer">
                            <p>
                                Каждый месяц сайт «Филин» посещают порядка 6 млн человек. Если каждый посетитель будет ежемесячно перечислять нам всего по 10 рублей, мы сможем запланировать еще больше командировок, выпустить новые спецпроекты и провести еще больше расследований, за которые нас все ценят.
                            </p>
                            <p>
                                Представьте, сколько всего можно будет сделать благодаря ежемесячным пожертвованиям в 10 рублей!
                            </p>
                            <p>
                                И кстати, 10 рублей — самая частая сумма пожертвования.
                            </p>
                        </div>
                    </div>
                    <div class="question-accordeon">
                        <div class="question-ask">
                            <span class="ask-sign"></span>
                            <p>
                                На что пойдут мои деньги?
                            </p>
                        </div>
                        <div class="question-answer">
                            <p>
                                Каждый месяц сайт «Филин» посещают порядка 6 млн человек. Если каждый посетитель будет ежемесячно перечислять нам всего по 10 рублей, мы сможем запланировать еще больше командировок, выпустить новые спецпроекты и провести еще больше расследований, за которые нас все ценят.
                            </p>
                            <p>
                                Представьте, сколько всего можно будет сделать благодаря ежемесячным пожертвованиям в 10 рублей!
                            </p>
                            <p>
                                И кстати, 10 рублей — самая частая сумма пожертвования.
                            </p>
                        </div>
                    </div>
                    <div class="question-accordeon">
                        <div class="question-ask">
                            <span class="ask-sign"></span>
                            <p>
                                Как отказаться от ежемесячных платежей?
                            </p>
                        </div>
                        <div class="question-answer">
                            <p>
                                Каждый месяц сайт «Филин» посещают порядка 6 млн человек. Если каждый посетитель будет ежемесячно перечислять нам всего по 10 рублей, мы сможем запланировать еще больше командировок, выпустить новые спецпроекты и провести еще больше расследований, за которые нас все ценят.
                            </p>
                            <p>
                                Представьте, сколько всего можно будет сделать благодаря ежемесячным пожертвованиям в 10 рублей!
                            </p>
                            <p>
                                И кстати, 10 рублей — самая частая сумма пожертвования.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-special-panels">
        <div class="container">
            <?php get_template_part('page_templates/home-page/section-special-panels') ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>