<?php
$donate = get_field('donate', 'option');
reset_rows(); //  It is very important to use here!!!
 if ( have_rows('section_large_banner', 'option') ): the_row();
        $large_image_url = get_sub_field('image');
        $large_link = get_sub_field('link');
?>
<section class="section-large-banner">
    <div class="container" style="position: relative;">
        <a href="<?= $large_link ?>" class="large-banner">
            <img src="<?= $large_image_url ?>">
        </a>
        <a href="/" class="logo">
            <img src="<?php echo get_template_directory_uri() ?>/img/logo-light.svg" alt="">
        </a>
        <a href="<?= $donate['url'] ?>" class="btn-special btn-special-lg">
            <span class="line"></span>
            <span class="word row-one">Поддержать</span>
            <span class="word row-two">проект</span>
        </a>
    </div>
</section>
<?php endif; ?>