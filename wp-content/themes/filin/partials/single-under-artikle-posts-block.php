<?php
    $post_id = get_the_ID();
    $category = get_the_category( $post_id );
    $term_link = get_term_link($category[0]->term_id); 
    $teaser = get_field('teaser');
?>
<div class="panel">
    <a href="<?php echo get_post_permalink() ?>" class="panel-image cover-container">
        <picture class="panel-picture cover">
            <?php the_post_thumbnail(); ?>
        </picture>
        <a href="<?= $term_link ?>" style="position: absolute;">
            <span class="panel-tag">
                <?= $category[0]->name ?>
            </span>
        </a>
    </a>
    <h2 class="panel-title">
        <a href="<?php echo get_post_permalink() ?>" class="panel-title-link">
            <span class="panel-main-title">
                <?= $teaser ?>
            </span>
            <span class="panel-description-text">
            </span>
        </a>
    </h2>
</div>