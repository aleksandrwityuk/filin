<?php 
    $post_id = get_the_ID();
    $category = get_the_category( $post_id );
    $term_link = get_term_link($category[0]->term_id);
    $teaser = get_field('teaser', $post_id); 
?>
<a href="<?= get_post_permalink() ?>" class="panel-image cover-container">
    <?= get_the_post_thumbnail($post_id, 'Medium') ?>
    <a href="<?= $term_link ?>" style="position: absolute;">
        <span class="panel-tag">
            <?= $category[0]->name ?>
        </span>
    </a>
</a>
<h2 class="panel-title">
    <a href="<?= get_post_permalink() ?>" class="panel-title-link">
        <?= $teaser ?>
    </a>
</h2>