<?php
    $post_id = get_the_ID();

    $author = get_autor_data($post_id);
?>
<div class="big-quote">
    <span class="big-quote-text">
        <?= get_the_content() ?>
    </span>
    <div class="big-quote-row">
        <!-- Display link if author is user -->
        <div class="big-quote-author">
            <div class="big-quote-author-image cover-container">
                <?= wp_get_attachment_image($author['author_image'], 'thumbnail') ?>
            </div>
            <span class="big-quote-author-name">
                <?= $author['first_name'] ?><br/><?= $author['last_name'] ?>
            </span>
        </div>
    </div>
</div>