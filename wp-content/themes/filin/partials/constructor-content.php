<?php
    // Check value exists.
    if( have_rows('constructor') ):

        // Loop through rows.
        while ( have_rows('constructor') ) : the_row();
            // Case: Paragraph layout.
            switch (get_row_layout()) {
                case 'text_field':
                    $text = get_sub_field('text');
                    echo $text;
                    break;
                case 'baner':
                    $image = get_sub_field('image');
                    $link = get_sub_field('link');
                    $under_photo_name = get_sub_field('under_photo_desctiption');
                    ?>
                    <div class="article-image">
                        <?php 
                        echo wp_get_attachment_image($image, 'full'); 
                        if ( $link !== '' ):
                        ?>
                        <span class="img-description">
                            Фото <a href="<?= $link; ?>"><?= $under_photo_name ?></a>
                        </span>
                        <?php endif; ?>
                    </div>
                    <?php
                    break;
                case 'full_width_big_quote_by_id_img':
                    $quote_id = intval( get_sub_field('quote_id') );
                    echo do_shortcode('[static-page-quote-img id="'.$quote_id.'"]');
                    break;
                case 'full_width_big_quote_by_id':
                    $quote_id = intval( get_sub_field('quote_id') );
                    echo do_shortcode('[static-page-quote id="'.$quote_id.'"]');
                    break;
                case 'banner_tech':
                    $image_id = get_sub_field('image');
                    $link =  get_sub_field('link');
                    echo do_shortcode('[static-page-wide-banner id="'.$image_id.'" link="'.$link.'"]');
                    break;
                /*------------------------only_mobile_ad_banner----------------------------*/
                case 'only_mobile_ad_banner':
                    $image_url = get_sub_field('mobile_ad_banner');
                    ?>
                        <aside class=" d-block d-sm-none article-aside">
                            <img src="<?= $image_url ?>" class="interactive" alt="" >
                        </aside>
                    <?php
                    break;
                case 'vrezka_type_1':
                    $text = get_sub_field('text');
                    ?>
                        <blockquote><p><?= $text; ?></p></blockquote>
                    <?php
                    break;
                case 'vrezka_type_2':
                    $text = get_sub_field('text');
                    ?>
                        <blockquote class="small-blockquote"><p><?= $text; ?></p></blockquote>
                    <?php
                    break;
                case 'separate_line':
                    echo do_shortcode('[separate-line]');
                    break;
                case 'read_also_block_without_image_double':
                    $ids = get_sub_field('add_two_posts');
                    $post_id_one = $ids[0];
                    $post_id_two = $ids[1];
                    echo do_shortcode('[read_also_block_without_image id-one="'.$post_id_one.'" id-two="'.$post_id_two.'"]');
                    break;
                case 'read_also_block_without_image':
                    $ids = get_sub_field('post_id');
                    $post_id_one = $ids[0];
                    echo do_shortcode('[static-page-read-also id="'.$post_id_one.'"]');
                    break;
                case 'read_also_block_with_image':
                    $post_id = (int)get_sub_field('post_id');
                    echo do_shortcode('[static-page-read-also-with-img id = "'.$post_id.'"]');
                    break;
                case 'quote_3000_background_img':
                    $quote_id = get_sub_field('quote_id');
                    echo do_shortcode('[static-page-large-text id="'.$quote_id.'"]');
                    break;
                case 'vrezka':
                    $largetext = get_sub_field('largetext');
                    $title = get_sub_field('title');
                    $content = get_sub_field('content');
                    echo do_shortcode('[static-page-vrezka largetext="'.$largetext. '" title="'.$title.'" content="'.$content.'"]');
                    break;
                case 'filin_mind_battle':
                    $qoute_ids = get_sub_field('quote_ids');
                    echo do_shortcode('[static-page-quotes-minds-battle ids="'.$qoute_ids.'"]');
                    break;
                case 'author_quote_large_block':
                    $quote_id = get_sub_field('quote_id');
                    echo do_shortcode('[static-page-author-quote-large id="'.$quote_id.'"]');
                    break;
                case 'slider':
                    $ids = get_sub_field('image_ids');
                    $ids = implode(',', $ids);
                    echo do_shortcode('[slider ids="'.$ids.'"]');
                    break;
            }
        // End loop.
        endwhile;

    // No value.
    else :
        // Do something...
    endif;
?>