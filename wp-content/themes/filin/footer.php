<footer class="footer" id="footer">
        <div class="container">
            <div class="row footer-row">
                <div class="col-xl-2 col-12">
                    <?php
                        reset_rows(); //  It is very important to use here!!!
						if( have_rows('social_networks', 'option') ): ?>
							<ul class="social-list">
								<?php while( have_rows('social_networks', 'option') ) : the_row(); 
                                     if ( get_sub_field('link')['title'] == 'vk'){
                                        $link = get_site_url();
                                    } else if ( get_sub_field('link')['title'] == 'rss') {
                                        $link = '';
                                    } else $link = get_permalink();
                                     ?>
										<li>
											<a target="<?= get_sub_field('link')['target'] ?>" href="<?= get_sub_field('link')['url'] ?><?php echo $link; ?>">
												<svg class="icon">
													<use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#<?= get_sub_field('image') ?>" />
												</svg>
											</a>
										</li>
									<?php
								endwhile; ?>
							</ul>
						<?php
						endif;
					?>
                </div>
                <div class="col-xl-8 col-12">
                    <?php
                        //Preparing and output menu
                        $menu = array(
                            'theme_location' => 'footer_menu',
                            'container'       => 'nav',
                            'container_class' => 'nav-menu', 
                            'menu_class'      => 'menu',
                            'depth'           => 1
                        );
                        wp_nav_menu($menu);
                    ?>
                </div>
                <div class="col-xl-2 col-12 col-right">
                    <span class="small-text">
                        © «Филин»,
                        <?= get_field('copywrite', 'option') ?>
                        <?= date("Y") == 2021 ? date("Y") : '2021-'.date("Y") ?>
                    </span>
                </div>
            </div>
            <div class="row footer-row-bottom">
                <div class="col-lg-4 col-12">
                    <span class="small-text">
                        <?= get_field('link_need_warning', 'option') ?>
                        При частичном или полном использовании материалов сайта гиперссылка на сайт онлайн-медиа «Филин» обязательна.
                    </span>
                </div>
                <div class="col-lg-4 col-12 col-center">
                    <div class="footer-images">
                        <img src="<?= get_template_directory_uri() ?>/img/icons/label.png" alt="">
                        <img src="<?= get_template_directory_uri() ?>/img/icons/tam-by.png" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-12 col-right">
                    <span class="small-text">
                        Редакция <a href="mailto:philin@gmail.com">philin@gmail.com</a>
                    </span>
                    <span class="small-text">
                        Редакция <a href="mailto:ad.philin@gmail.com">ad.philin@gmail.com</a>
                    </span>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php
if ( have_rows('site_brand', 'option') ): the_row(); 
    $background_img = get_field('background_page_ad_img', 'option');
    if ($background_img !== false):
?>
</div>
<?php endif; ?>
<?php endif; ?>
<?php wp_footer(); ?>
    <style>
        .section-panels .row > div {
            transition: 0.3s ease;
        }
        .loading {
            opacity: 0;
        }
    </style>

</body>
</html>
