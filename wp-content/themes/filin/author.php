<?php get_header(); ?>
<?php  
    $autor_id = get_the_author_meta( 'ID' );
    $autor_description = get_the_author_meta('description');
    $autor_description = explode('**', $autor_description);
    $autor_firstname = get_the_author_meta('first_name');
    $autor_lastname = get_the_author_meta('last_name');
    $autor = new WP_User( $autor_id );
    $role = wp_roles()->get_names()[ $autor->roles[0] ];
    $url = get_avatar_url( $autor_id, array(
        'size' => 48,
        'default'=>'identicon',
    ));
?>

<main class="main">

    <section class="section-author section-author-top">
        <div class="container">
            <div class="author-quote author-block large">
                <div class="author-row">
                    <span class="author-image cover-container">
                        <img src="<?= $url; ?>" class="cover" alt="">
                    </span>
                    <span class="author-name">
                        <?= $autor_firstname; ?> <br/> <?= $autor_lastname; ?>
                    </span>
                </div>
                <div class="role"><span><?= $role ?></span></div>
                <div class="author-quote-text">
                    <span class="author-quote-text-main">
                        <?= $autor_description[0] ?> </span>
                    <span class="author-quote-text-desc">
                        <?= $autor_description[1] ?>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section class="section-panels">
        <div class="container">
            <div class="row">
            <?php
            $itr = 0;
            if (have_posts()):
                while ( have_posts() ): the_post();
                    if ( $itr < 9 ):
                        get_template_part('partials/loop-panel-post');
                        $itr++;
                    endif;
                endwhile;
            endif;
            ?>
            </div>
        </div>
    </section>
    <section class="section-large-banner">
        <div class="container">
            <div class="large-banner">
                <a href="/" class="logo">
                    <img src="<?= get_template_directory_uri() ?>/img/logo-light.svg" alt="">
                </a>
                <img src="<?= get_template_directory_uri() ?>/img/icons/arrow-rectangle.svg" alt="" class="large-banner-icon">
                <?php $donate = get_field('donate', 'option'); ?>
                <a target="<?= $donate['target'] ?>" href="<?= $donate['url'] ?>" class="btn-special btn-special-lg">
                    <span class="line"></span>
                    <span class="word row-one">Поддержать</span>
                    <span class="word row-two">проект</span>
                </a>
            </div>
        </div>
    </section>
    <section class="section-panels" id="loadmore">
        <div class="container">
            <div class="row">
            <?php
            $itr = 0;
            if (have_posts()):
                while ( have_posts() ): the_post();
                    if ( $itr > 8 && $itr < 18 ):
                        get_template_part('partials/loop-panel-post');
                    endif;
                    $itr++;
                endwhile;
            endif;
            ?>
            </div>
            <div class="button-block-center">
                <button :disabled="disabled" class="btn-outlined btn-bright" v-if="show" id="btn-loadmore" @autor="<?= $autor_id; ?>">
                    <svg class="icon">
                        <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#reload" />
                    </svg>
                    <span class="text">
                        Загрузить еще
                    </span>
                </button>
            </div>
        </div>
    </section>
    <section class="section-special-panels">
        <div class="container">
            <?php get_template_part('page_templates/home-page/section-special-panels', null, 
        array('location' => 'footer')) ?>
        </div>
    </section>
</main>

<?php get_footer() ?>