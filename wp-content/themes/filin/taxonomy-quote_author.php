<?php get_header(); ?>
<?php 
    $author = get_queried_object();
    $autor_id = $author->term_id;
    $author_name = explode(' ' , $author->name);
    $autor_firstname = $author_name[0];
    $autor_lastname = $author_name[1];
    $term_author_settings = get_field('setting_author', $author);
    $url = wp_get_attachment_image_url( $term_author_settings['image'] );
    $role = $term_author_settings['author_role'];
    $autor_description = $author->description;
    $autor_othor_description = $term_author_settings['description_two'];
?>
<main class="main" id="srchresult">

    <section class="section-author section-author-top">
        <div class="container">
            <div class="author-quote author-block large">
                <div class="author-row">
                    <span class="author-image cover-container">
                        <img src="<?= $url; ?>" class="cover" alt="">
                    </span>
                    <span class="author-name">
                        <?= $autor_firstname; ?> <br/> <?= $autor_lastname; ?>
                    </span>
                </div>
                <div class="role"><span><?= $role ?></span></div>
                <div class="author-quote-text">
                    <span class="author-quote-text-main">
                        <?= $autor_description ?> </span>
                    <span class="author-quote-text-desc">
                        <?= $autor_othor_description ?>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section class="section-panels">
        <div class="container">
            <div class="row">
            <?php
            $itr = 0;
            if (have_posts()):
                while ( have_posts() ): the_post();
                    if ( $itr < 9 ):
                        get_template_part('partials/loop-panel-post');
                        $itr++;
                    endif;
                endwhile;
            endif;
            ?>
            </div>
        </div>
    </section>
    <?php
        get_template_part('partials/large-banner');
    ?>
    <section class="section-panels" id="loadmore">
        <div class="container">
            <div class="row">
            <?php
            $itr = 0;
            if (have_posts()):
                while ( have_posts() ): the_post();
                    if ( $itr > 8 && $itr < 18 ):
                        get_template_part('partials/loop-panel-post');
                    endif;
                    $itr++;
                endwhile;
            endif;
            ?>
            </div>
            <div class="button-block-center">
                <button :disabled="disabled" class="btn-outlined btn-bright" v-if="show" id="btn-loadmore" @autor="<?= $autor_id; ?>">
                    <svg class="icon">
                        <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#reload" />
                    </svg>
                    <span class="text">
                        Загрузить еще
                    </span>
                </button>
            </div>
        </div>
    </section>
    <section class="section-special-panels">
        <div class="container">
            <?php get_template_part('page_templates/home-page/section-special-panels', null, 
        array('location' => 'footer')) ?>
        </div>
    </section>
</main>

<?php get_footer() ?>