<?php get_header(); ?>
<?php
if ( have_posts() ) : the_post(); 
    $this_post_id = get_the_ID();
    $description = get_field('description', $this_post_id);
    $category = get_the_category( $this_post_id );
    $author_column_cat = has_category( 'author_column', $this_post_id );
    $date = get_the_date( 'j F Y', $this_post_id ); 
    $term_link = get_term_link($category[0]->term_id);
    $article_source = '';
    $article_source_link = '';
    $author = get_autor_data($this_post_id);
    $aterms = get_the_terms( $this_post_id, 'quote_author' );
    
    if ($aterms && $author) {
        $article_source = $author['first_name'] . ' ' . $author['last_name'] . ', ' . $author['role']; 
        $article_source_link = get_term_link($aterms[0]->term_id);
    } else {
        $article_source = get_field('source')['author'];
        $article_source_link = get_field('source')['link'];
    }
    $views_counter = get_field('views_counter');
    update_field('views_counter', $views_counter++);
    $post_format = get_field('post_format', $post_id);
    // if donuts
    if ($post_format == 'donate'):
        include(get_template_directory() . '/partials/donuts.php');
    else:
?>
<?php 
    if ( have_rows('ad_banner_at_head_interactive') ): the_row();
        $image_url = get_sub_field('image');
        $link = get_sub_field('link');
        $mobile_image_url = get_sub_field('image_mobile');
        ?>
        <section class="section-head">
            <div class="container">
                <div class="d-none d-sm-block banner-full-width">
                    <a href="<?php echo $link; ?>">
                        <img src="<?php echo $image_url; ?>">
                    </a>
                </div>
                <div class="d-block d-sm-none interactive-block">
                    <?php $image_url = get_field('interactive_block_mobile_banner', $this_post_id) ?>
                    <a href="<?= $link ?>">
                        <img src="<?= $mobile_image_url ?>" class="interactive" alt="" >
                    </a>
                </div>
            </div>
        </section>
        <?php
    endif;
?>
    <main class="main">
        
        <section class="section-article">
            <div class="container">
                <div class="article-container">
                    <div class="row">
                        <div class="col-lg-9 col-12" id="srchresult">
                            <div class="article-col-inner">
                                <div class="main-article-info">
                                    <a href="<?= $term_link ?>">
                                        <span class="article-category">
                                            <?= $category[0]->name; ?>
                                        </span>
                                    </a>
                                    <span class="article-date">
                                        <?= $date; ?>
                                    </span>
                                    <?php if(!$author_column_cat):  ?>
                                    <a href="<?= $article_source_link ?>">
                                        <span class="article-info">
                                            <?= $article_source ?>
                                        </span>
                                    </a>
                                <?php endif; ?>
                                </div>
                                <h1 class="article-title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="article-description">
                                    <p>
                                       <?= $description; ?>
                                    </p>
                                </div>
                                <?php if($author_column_cat): 
                                    $author_image_url = wp_get_attachment_image_url( $author['author_image'] );
                                    $author_link = $author['author_archive_link'];
                                    $role = $author['role'];
                                ?>
                                    <div class="author-quote author-block single large">
                                        <div class="author-row">
                                            <span class="author-image cover-container">
                                                <a href="<?= $author_link ?>" class="panel-title-link">
                                                    <img src="<?= $author_image_url ?>" class="cover" alt="">
                                                </a>
                                            </span>
                                            <span class="author-name"><?= $author['first_name'] ?><br/><?= $author['last_name'] ?></span>
                                            <div class="role"><span><?= $role ?></span></div>
                                        </div>
                                    </div>
                                <?php endif;

                                $flag_image_width = get_field('make_full_width_featured_image', $this_post_id);
                                $width_class = '';
                                if ($flag_image_width) {
                                   $width_class = 'wide';
                                }
                                ?>
                                <div class="article-image <?= $width_class ?>">
                                    <?php the_post_thumbnail(); ?>
                                    <span class="img-description">
                                        Фото <a href="/">РАР</a>
                                    </span>
                                </div>
                                <div class="content"><!--Here is a start of ACF layouts-->
                                <?php get_template_part(
                                    'partials/constructor-content', 
                                    null,
                                    array( 
                                      'post-id' => $this_post_id
                                    )); 
                                ?>
                                </div><!-- End class content -->
                                <?php get_template_part('partials/article-footer', null,
                                    array( 
                                      'post-id' => $this_post_id
                                    )); 
                                ?>
                            </div><!--end class="article-col-inner"-->
                        </div>
                        <div class="d-none d-sm-block col-lg-3 col-12">
                            <aside class="article-aside sticky">
                                <?php
                                $itr = 0 ;
                                // Check value exists.
                                if( have_rows('sidebar_constructor', 'option') ):
                                    // Loop through rows.
                                    while ( have_rows('sidebar_constructor', 'option') ) : the_row();
                                        if ($itr == 1) {
                                            $pinned_posts = get_field('pinned_posts', 'option');
                                            $recent_post = filinn_get_recent_posts(1);
                                            $pinned_posts[] = $recent_post[0];
                                            $num = 0;
                                            $pin = 0;
                                            foreach ($pinned_posts as $post):
                                                $post_id = get_the_ID();
                                                if ($post_id == $this_post_id) {
                                                    $recent_post = filinn_get_recent_posts(1,1);
                                                    $post = $recent_post[0];
                                                }
                                                setup_postdata($post);
                                                $post_format = get_field('post_format', $post_id);
                                                 if ($post_format == 'quote') : ?>
                                                    <div class="author-quote">
                                                        <?php get_template_part('partials/loop-main-quote') ?>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="panel">
                                                        <?php get_template_part('partials/loop-main-default'); ?>
                                                    </div>
                                                <?php endif;
                                            endforeach;
                                        } else {
                                            // Case: Paragraph layout.
                                            switch (get_row_layout()) {
                                                case 'advert_sidebar_banner':
                                                    $banner_sidebar_id = get_sub_field( "image");
                                                    echo wp_get_attachment_image( $banner_sidebar_id['ID'], 'full', "", ["class" => "interactive","alt"=>"interactive-sidebar"]);
                                                    break;
                                                case 'rss':
                                                    get_template_part('page_templates/home-page/section-special-panels');
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        $itr++;
                                    endwhile;
                                endif;
                                ?>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-more">
            <div class="container">
                <?php
                if ( have_rows('ad_banner_at_bottom_interactive', $this_post_id) ): the_row();
                    $b_image_url = get_sub_field('image');
                    $b_link = get_sub_field('link');
                    $b_mobile_image_url = get_sub_field('image_mobile');
                ?>
                <div class="d-none d-sm-block interactive-block">
                    <a href="<?= $link ?>">
                        <img src="<?= $b_image_url ?>" class="interactive" alt="" >
                    </a> 
                </div>
                <div class="d-block d-sm-none interactive-block">
                    <a href="<?= $b_link ?>">
                        <img src="<?= $b_mobile_image_url ?>" class="interactive" alt="" >
                    </a>
                </div>
                <?php endif;?>
                <!-- ** Read more block ** -->
                <div class="border-block">
                    <span class="border-name-block">
                        <span class="border-name">
                            Читайте ещё
                        </span>
                    </span>
                    <?php
                        $ad_flag = get_field('single_add_ad_banner_read_more', $this_post_id);
                        reset_rows(); //  It is very important to use here!!!
                        if ( $ad_flag && have_rows('readmore_ad_banner', 'option') ): the_row();
                        $b_image_url = get_sub_field('image');
                        $b_link = get_sub_field('link');
                        $b_mobile_image_url = get_sub_field('image_mobile');
                        ?>
                        <div class="d-none d-sm-block interactive-block">
                        <a href="<?= $link ?>">
                            <img src="<?= $b_image_url ?>" class="interactive" alt="" >
                        </a> 
                        </div>
                        <div class="d-block d-sm-none interactive-block">
                            <a href="<?= $b_link ?>">
                                <img src="<?= $b_mobile_image_url ?>" class="interactive" alt="" >
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="border-block-inner">
                            <div class="row">
                                <?php
                                $posts_num_flag = get_field('how_many_posts_show', $this_post_id);
                                $num = ($posts_num_flag)? 4 : 8;
                                if ( wp_is_mobile() ) { $num = 4; }
                                $the_query = new WP_Query( 
                                    array(
                                        'post_type' => 'post',
                                        'posts_per_page' => $num,
                                        'ignore_sticky_posts' => 2,
                                        'meta_key' => 'views_counter',
                                        'orderby' => array( 'date' => 'DESC', 'meta_value' => 'DESC')
                                    ));
                                if ( $the_query->have_posts() ):
                                    while ( $the_query->have_posts() ):
                                        $the_query->the_post();
                                ?>
                                    <div class="col-md-3 col-sm-6 col-12">
                                        <?php
                                        get_template_part('partials/single-under-artikle-posts-block');
                                        ?>
                                    </div>
                                <?php 
                                    endwhile;
                                    wp_reset_postdata(); 
                                endif; 
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <?php
        get_template_part('partials/large-banner');
        ?>

    </main>
<?php endif; // end if donuts ?>
<?php endif; ?>
<?php get_footer(); ?>
