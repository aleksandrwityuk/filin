<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<?php get_header(); ?>

<main class="main">   
    <section class="section-article">
        <div class="container">
            <div class="error-404 not-found row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <h1>404</h1>
                    <h2>Страница не найдена</h2>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="button-block-center">
                <div class="home-404">
                    <a href="/">
                        <h1>На главную</h1>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>