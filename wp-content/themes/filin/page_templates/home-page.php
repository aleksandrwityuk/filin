<?php
/* 
Template Name: Home page
*/
get_header(); ?>
<main class="main" id="srchresult">
    <?php get_template_part('page_templates/home-page/section-main') ?>
    <?php get_template_part('page_templates/home-page/section-quote') ?>
    <?php get_template_part('page_templates/home-page/section-panels') ?>
    <?php
    $show_rss = get_field('hidden_rss_block','option');
    if(!$show_rss): ?>
    <section class="section-special-panels">
        <div class="container">
        <?php get_template_part('page_templates/home-page/section-special-panels', null, 
        array('location' => 'footer')) ?>
        </div>
    </section>
    <?php endif; ?>
</main>
<?php get_footer() ?>