<?php
remove_shortcode('static-page-quote');
remove_shortcode('static-page-wide-banner');
remove_shortcode('read_also_block_without_image');
remove_shortcode('static-page-read-also-with-img');
remove_shortcode('static-page-read-also');
remove_shortcode('static-page-large-text');
remove_shortcode('static-page-vrezka');
remove_shortcode('static-page-quotes-minds-battle');
remove_shortcode('static-page-author-quote-large');
remove_shortcode('slider');

add_shortcode('static-page-quote-img', 'filin_staticPage_quote_shortcode_img');
add_shortcode('static-page-quote', 'filin_staticPage_quote_shortcode');
add_shortcode('static-page-wide-banner', 'filin_staticPage_wide_banner_shortcode');
add_shortcode('read_also_block_without_image', 'filin_read_also_block_without_image_shortcode');
add_shortcode('static-page-read-also-with-img', 'filin_Read_Also_Img_shortcode');
add_shortcode('static-page-read-also', 'filin_Read_Alsoshortcode');
add_shortcode('static-page-large-text', 'filin_Large_text');
add_shortcode('static-page-vrezka', 'filin_vrezka_shortcode');
add_shortcode('static-page-quotes-minds-battle', 'filin_Mind_Battle');
add_shortcode('static-page-author-quote-large', 'filin_Author_quote_large');
add_shortcode('slider', 'filin_slider');
add_shortcode('separate-line', 'filin_separate_line');

function filin_separate_line( $attr ) {
    $output = '<span class="separate-line"></span>';
    return $output;
}


function filin_staticPage_quote_shortcode($attr) {
    $atts = shortcode_atts( array(
        'id'   => 1954   
    ), $atts );
    if (isset($attr['id'])) {
        $id = $attr['id'];
    }
    
    $the_query = new WP_Query( 
        array(
            'post_type' => 'quote',
            'order'          => 'ASC',
            'posts_per_page' => 1,
            'p'    => $id
        ));
   
    if ( $the_query->have_posts() ):
        while ( $the_query->have_posts() ):
            $the_query->the_post();
            ob_start();
            get_template_part('partials/full-width-big-quote');
            $output = ob_get_clean();
        endwhile;
        wp_reset_postdata(); 
    endif;
    return $output;
}

function filin_staticPage_quote_shortcode_img( $attr ) {

    $atts = shortcode_atts( array(
        'id'   => 1954   
    ), $atts );
    if (isset($attr['id'])) {
        $id = $attr['id'];
    }
    
    $the_query = new WP_Query( 
        array(
            'post_type' => 'quote',
            'order'          => 'ASC',
            'posts_per_page' => 1,
            'p'    => $id
        ));
   
    if ( $the_query->have_posts() ):
        while ( $the_query->have_posts() ):
            $the_query->the_post();
            ob_start();
            get_template_part('partials/full-width-big-quote-img');
            $output = ob_get_clean();
        endwhile;
        wp_reset_postdata(); 
    endif;
    return $output;
}
// [static-page-wide-banner id="2134" link="#"]
function filin_staticPage_wide_banner_shortcode($attr) {
    $atts = shortcode_atts( array(
        'id'   => 2134,
        'link'  => '#'
    ), $atts );

    $image_url = wp_get_attachment_url( $attr['id'] );
    $link = $attr['link'];

    $banner = '<div class="d-none d-sm-block banner-tech">
        <a href="'.$link.'"><img src="'. $image_url . '" alt=""></a>
    </div>';
    return $banner;
}

// "Читайте также" block [read_also_block_without_image id-one="2046" id-two="2101"]

function filin_read_also_block_without_image_shortcode ( $attr ) {

    $post_id_one = $attr['id-one'];
    $post_id_two = $attr['id-two'];

    $args = array(
       'post_type' => 'post',
       'post_status' => 'publish',
       'post__in'      => array($post_id_one, $post_id_two),
       'orderby' => 'post__in'
    );
    // The Query
    $the_query = new WP_Query( $args );

    $border_block = '<div class="border-block">
        <span class="border-name-block">
            <span class="border-name">
                Читайте также
            </span>
        </span>
        <div class="border-block-inner">
            <div class="row">';
                if ( $the_query->have_posts() ):
                    $itr = 0;
                    while ( $the_query->have_posts() ):
                        $border_block .= '<div class="col-sm-6 col-12">';
                        $the_query->the_post();
                        if ($itr < 2) { }
                        else {
                            $cats = get_the_category($id);
                            ob_start();
                                get_template_part(
                                    'partials/loop-static-page-read-more', null, 
                                    array( 
                                      'category_name' => $cats[0]->name
                                    ));
                            $output = ob_get_clean();
                            $border_block .=  $output;
                        }
                            $itr++;
                            $border_block .= '</div>';
                    endwhile;
                    wp_reset_postdata();
                endif;
                $border_block .= '</div></div></div>';
    return $border_block;
}

// [static-page-read-also-with-img id = "2038"]
function filin_Read_Also_Img_shortcode($attr) {

    $post_id = $attr['id'];
    $the_query = new WP_Query( 
        array(
            'post_type'   => 'post',
            'posts_per_page' => 1,
            'p'    => $post_id,
            'post_status' => 'publish'
        ));
   $id = array();
    if ( $the_query->have_posts() ):
        while ( $the_query->have_posts() ):
            $the_query->the_post();
            $id[] = get_the_ID();
            $cats = get_the_category($id);
            ob_start();
            get_template_part('partials/read-also-with-img', null, 
                                array( 
                                  'category_name' => $cats[0]->name
                                ));
            $output = ob_get_clean();
        endwhile;
        wp_reset_postdata(); 
    endif;
    //$output = json_encode($id);
    return $output;
}

// [static-page-read-also id="2046"]

function filin_Read_Alsoshortcode ($attr) {
    $post_id = $attr['id'];
    $the_query = new WP_Query( 
        array(
            'post_type'   => 'post',
            'posts_per_page' => 1,
            'p'    => $post_id,
            'post_status' => 'publish'
        ));
    $border_block = '<div class="border-block border-block-small">
                                        <span class="border-name-block">
                                            <span class="border-name">
                                                Читайте также
                                            </span>
                                        </span>';
    if ( $the_query->have_posts() ):
        while ( $the_query->have_posts() ):
            $the_query->the_post();
            $id = get_the_ID();
            $cats = get_the_category($id);
            ob_start();
            get_template_part('partials/loop-static-page-read-more', null, 
                                array( 
                                  'category_name' => $cats[0]->name
                                ));
            $output = ob_get_clean();
            $border_block .= $output;
        endwhile;
        wp_reset_postdata(); 
    endif;
    $border_block .= '</div>';
    return $border_block;
}
// [static-page-large-text id="2053"]
function filin_Large_text($attr) {

    $atts = shortcode_atts( array(
        'id'   => 2053,   
    ), $atts );

    $args = [
        'post_type' => 'quote',
        'order'          => 'ASC',
        'posts_per_page' => 1,
        'p'    => $attr['id']
    ];
    
    $the_query = new WP_Query($args);
        
    if ( $the_query->have_posts() ):
        while ( $the_query->have_posts() ):
            $the_query->the_post();
            $largetext = get_field('large-text', get_the_ID());
            $output ='<div class="bg-block cover-container">
            <img src="' . get_the_post_thumbnail_url() . '" alt="" class="cover">
                <div class="bg-block-inner">
                    <span class="large-text">'
                        . $largetext .
                    '</span>
                    <h3 class="bg-block-title">'
                        . get_the_title() .
                    '</h3>
                    <div class="text">'
                        . get_the_content() .
                    '</div>
                </div>
            </div>';
            
        endwhile;
        wp_reset_postdata(); 
    endif;
    return $output;
}

function filin_vrezka_shortcode($attr) {
    $atts = shortcode_atts( array(
        'largetext'   => '1350',  
        'title'  => 'долларов',
        'content' => 'составляет сейчас средняя зарплата в Польше.'
    ), $atts );

    $output = '<div class="block-text">
            <span class="large-text">'
               . $attr['largetext'] .
            '</span>
            <h3 class="bg-block-title">'
                . $attr['title'] .
            '</h3>
            <div class="text">
                <p>'
                   . $attr['content'] .
                '</p>
            </div>
        </div>';

        return $output;
}
// [static-page-quotes-minds-battle ids="1941,1954"]
function filin_Mind_Battle($attr) {

    $atts = shortcode_atts( array(
        'id'   => ' ',  
    ), $atts );

    if (isset($attr['ids'])) {
        $rel = explode(",", $attr['ids']);
        foreach ($rel as &$value) {
            $value = intval($value);
        }
    }

    $args = [
        'post_type' => 'quote',
        'order'          => 'ASC',
        'post__in' => $rel
    ];
    
    $the_query = new WP_Query($args);
    $output = '<div class="quotes-block">
                    <div class="section-quote-inner">
                        <span class="section-quote-title">
                            <span class="section-quote-title-text">Битва мнений</span>
                        </span> ';
        if ( $the_query->have_posts() ):
            while ( $the_query->have_posts() ):
                $the_query->the_post();
                ob_start();
                get_template_part('partials/loop-static-page-qoutes-battle-mind');
                $output .= ob_get_clean();
            endwhile;
            wp_reset_postdata(); 
        endif;
    $output .= '</div></div>';

    return $output;
}

// [static-page-author-quote-large id="1954"]
function filin_Author_quote_large($attr) {
    $atts = shortcode_atts( array(
        'id'   => 2053,   
    ), $atts );

    $args = [
        'post_type' => 'quote',
        'p'    => $attr['id']
    ];
    
    $the_query = new WP_Query($args);
        
    if ( $the_query->have_posts() ):
        while ( $the_query->have_posts() ):
            $the_query->the_post();
            ob_start();
            get_template_part('partials/author-quote-large');
            $output = ob_get_clean();
        endwhile;
        wp_reset_postdata(); 
    endif;
    return $output;
}

// [slider ids="2074,2075,2076,2077,2078,2079,2080,2081,2082"]
function filin_slider($attr) {

    if (isset($attr['ids'])) {
        $rel = explode(",", $attr['ids']);
        foreach ($rel as &$value) {
            $value = intval($value);
        }
    }
    
    $output = '<div class="slider-container">
        <div class="slider">';
        foreach ($rel as $attach_id) {
            $output .= '<div class="slide-item">'
                . wp_get_attachment_image( $attach_id,  'full' ) .
            '</div>';
        }
            
       $output .= '</div>
        <div class="slider-thumbnails">';
        foreach ($rel as $attach_id) {
            $output .= '<div class="thumbnails-item cover-container">'
                . wp_get_attachment_image( $attach_id ) .
            '</div>';
        }
        $output .= '</div>
    </div>';

    return $output;
}