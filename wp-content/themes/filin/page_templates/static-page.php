<?php
/*
Template Name: Static page template
Template Post Type: page
*/
get_header(); ?>
    <main class="main">
        <section class="section-article">
            <div class="container">
                <div class="article-container static-page-container">
                    <div class="row">
                        <div class="col-12">
                            <div class="article-col-inner">
                                <?php  $the_id = ''; 
                                    if ( have_posts() ) : the_post(); $the_id = get_the_ID(); 
                                ?>
                                <h1 class="article-title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="article-description">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="article-image">
                                    <?php the_post_thumbnail(); ?>
                                    <span class="img-description">
                                        Фото <a href="/">РАР</a>
                                    </span>
                                </div>
                                <div class="content"><!--Here is a start of ACF layouts-->
                                <?php get_template_part('partials/constructor-content', null,
                                    array( 
                                      'post-id' => $the_id
                                    ));
                                ?>
                                </div><!-- End class content -->
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php get_template_part('partials/article-footer', null,
                    array( 
                      'post-id' => $the_id
                    )); 
                ?>
            </div>
        </section>

        <?php
        get_template_part('partials/large-banner');
        ?>

    </main>
<?php get_footer(); ?>