<?php
    $quotes = get_field('quotes');
    if ($quotes) :
?>
<section id="likes" class="section-quote">
    <div class="container">
        <div class="section-quote-inner">
            <span class="section-quote-title">
                <span class="section-quote-title-text">Цитаты Дня</span>
            </span>
            <div class="row quote-row">
                <?php foreach ($quotes as $post) : 
                        setup_postdata($post);
                        get_template_part('partials/loop-big-quote');
                        ?>
                <?php endforeach;
                    wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>