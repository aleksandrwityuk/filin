<section class="section-main">
    <div class="container">
        <div class="row">
            <?php
                $avert = get_field('advert');
                $custom_avert_code = trim(get_field('ad_banner_with_codding'));
                $pinned_posts = get_field('pinned_posts', 'option');   
                if ($pinned_posts) :
                    $i = 0;
                    foreach ($pinned_posts as $post) :
                        setup_postdata($post);
                        $post_id = get_the_ID();
                        $post_format = get_field('post_format', $post_id);

                        if ($i > 2 && $avert['image']) :
                            break;
                        elseif ($i > 4) :
                            break;
                        endif;
                        
                        if ($i == 0) : ?>
                            <div class="col-lg-6 col-sm-6 col-12">
                        <?php elseif ($i%2 != 0) : ?>
                            <div class="col-lg-3 col-sm-6 col-12">
                        <?php endif;

                            if ($post_format == 'quote' || has_category( 'author_column', $post_id)) : ?>
                                <div class="author-quote">
                                    <?php get_template_part('partials/loop-main-quote') ?>
                                </div>
                            <?php else : ?>
                                <div class="<?= $i == 0 ? 'panel-large' : 'panel' ?>">
                                    <?php get_template_part('partials/loop-main-default'); ?>
                                </div>
                            <?php endif; ?>

                        <?php if ($i == 0 || $i%2 == 0) : ?>
                            </div>
                        <?php endif; ?>

                        <?php $i++;
                    endforeach;
                endif;
                wp_reset_postdata();
            ?>
            <?php  if (strlen($custom_avert_code) > 0): $avert = null; ?>
                <div class="col-lg-3 col-sm-center col-12">
                    <?= $custom_avert_code ?>
                </div>
            <?php elseif($avert['image']) :  ?>
                <div class="col-lg-3 col-sm-center col-12">
                    <a target="<?= $avert['link']['target'] ?>" href="<?= $avert['link']['url'] ?>">
                        <?= wp_get_attachment_image($avert['image'], 'Medium_large') ?>
                    </a>
                </div>
            <?php else: ?>
                <div class="col-lg-3 col-sm-center col-12">
                <?php
            $recent_posts = filinn_get_recent_posts(2);
            foreach ($recent_posts as $post):
                setup_postdata($post);
                $post_id = get_the_ID();
                $post_format = get_post_format();
                 if ($post_format == 'quote') : ?>
                    <div class="author-quote">
                        <?php get_template_part('partials/loop-main-quote') ?>
                    </div>
                <?php else : ?>
                    <div class="panel">
                        <?php get_template_part('partials/loop-main-default'); ?>
                    </div>
                <?php endif;
            endforeach;  wp_reset_postdata();
            ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>