<?php
    $rss_items = get_Rss();
    $rss_posts = array();
    $itr = 3;
    foreach ($rss_items as $item) {
        if ( $itr > 0 ) {
            $enclosure =  (array)$item->enclosure;
            $image_url = $enclosure['@attributes']['url'];
            if ( $image_url != null ) {
                $rss_posts[] = array(
                    'title' => $item->title,
                    'content' => $item->content,
                    'image_url' => $image_url,
                    'link' => $item->link
                );
                $itr--;
            } else {
                continue;
            }

        }
    }
    $back_img_url = get_field('rss_background_image', 'option');
    $back_img_url = ($back_img_url) ? $back_img_url : get_template_directory_uri() . '/img/bg.jpeg';
?>
<div class="section-special-column" style="background-image: url(<?= $back_img_url ?>)">
    <div class="row">
        <div class="col-lg-3 col-12">
            <div class="special-panels-logo-block">
                <?php
                    $logo = ($args['location'] == 'footer')? get_template_directory_uri() .'/img/solidarnasts.svg' : get_template_directory_uri() .'/img/solidarnasts-red.svg' ;
                ?>
                <img src="<?= $logo ?>" class="special-panels-logo" alt="">
            </div>
        </div>
        <div class="col-lg-5 col-sm-6 col-12">
            <div class="panel-newspaper panel-newspaper-large">
                <?php 
                $rss_post = $rss_posts[0];
                include(locate_template('partials/loop-rss-post.php')) ?>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-12">
            <div class=" panel-newspaper">
                <?php
                $rss_post = $rss_posts[1];
                include(locate_template('partials/loop-rss-post.php')) ?>
            </div>
            <div class=" panel-newspaper">
                <?php
                $rss_post = $rss_posts[2];
                include(locate_template('partials/loop-rss-post.php')) ?>
            </div>
        </div>
    </div>
</div>
