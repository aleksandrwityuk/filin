<?php
    $breadcrumbs = get_field('to_hide_posets_date_author');
    $main_posts = get_field('main_posts');
    $banner_337x281 = $main_posts['banner_337x281'];
    $numberposts = 18;
    $post_cat = 0;
    if (isset($args['cat'])) {
        $post_cat = $args['cat'];
    }

    //Condition if exist banner
    if ($banner_337x281['image']) :
        $numberposts = $numberposts - 1;
    endif;

    //Count of sticky posts
    $args = [
        'post__in' => get_option('sticky_posts'),
        'posts_per_page' => 9,
        'cat'    => $post_cat,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'suppress_filters' => true,
        'post_status' => 'publish'
    ];
    $sticky_posts = new WP_Query($args);
    $sticky_posts_count = $sticky_posts->found_posts;

    $posts_per_page = $numberposts - $sticky_posts_count;

    //Create main query
    $posts = array(
        'posts_per_page' => $posts_per_page,
        'cat'    => $post_cat,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'ignore_sticky_posts' => 0,
        'suppress_filters' => true,
        'post_status' => 'publish'
    );
    $query = new WP_Query($posts);

    //Count post in selected query
    $query_count = $query->found_posts; 

    //echo $query->post_count;

    //Create parts of query
    $first_part_posts = array_slice($query->posts, 0, floor($numberposts/2));
    $second_part_posts = array_slice($query->posts, floor($numberposts/2));

    //Add variables of post for JS
    wp_localize_script('script', 'load_more', array(
        'post_cat'      => $post_cat,
        'numberposts'   => $numberposts,
        'query_count'   => $query_count
    ));
    
?>
<section class="section-panels">
    <div class="container">
        <div class="row">
            <?php
                $i = 0;
                foreach($first_part_posts as $post) {
                    if ($i == 2 && $banner_337x281['image']) : ?>
                        <div class="col-lg-4 col-md-6 col-12">
                            <?= wp_get_attachment_image( $banner_337x281['image'], 'large', '', ['class'=>'small-banner']  ) ?>
                        </div>
                    <?php endif;
                    echo loop_post($post, '', $breadcrumbs);
                    $i++;
                }
                wp_reset_postdata();
            ?>
        </div>
    </div>
</section>

<?php 
if ($main_posts['donate'] || isset($args['cat'])) : 
    get_template_part('partials/large-banner');
endif; 
//Count posts in second part
if (count($second_part_posts) >= 1) : ?>
    <section class="section-panels" id="loadmore">
        <div class="container">
            <div class="row" ref="posts">
                <?php
                    foreach($second_part_posts as $post) {
                        echo loop_post($post, '', $breadcrumbs);
                    }
                    wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php 
        //Check for display
        if ($query_count > $numberposts) : ?>
            <div class="button-block-center">
                <button :disabled="disabled" class="btn-outlined btn-bright" v-if="show" id="btn-loadmore" 
                <?php
                if (isset($args['cat'])) {
                    echo '@cat="'.$args['cat'].'"';
                }
                echo ' @brdcrbs = "'. $breadcrumbs .'"';
                ?>>
                    <svg class="icon">
                        <use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#reload" />
                    </svg>
                    <span class="text">
                        Загрузить еще
                    </span>
                </button>
            </div>
        <?php endif; ?>
    </section>
<?php endif; ?>