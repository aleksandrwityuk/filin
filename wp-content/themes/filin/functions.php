<?php

//const 

//setup

//includes
include(get_template_directory() . '/includes/front/enqueue.php');
include(get_template_directory() . '/includes/ednpoints.php');
include(get_template_directory() . '/includes/setup.php');
include(get_template_directory() . '/includes/strings_translation.php');
include(get_template_directory() . '/includes/additional_func.php');
include(get_template_directory() . '/includes/post_type_and_tax.php');
include(get_template_directory() . '/includes/walker.php');
include(get_template_directory() . '/page_templates/static-page/shortcodes.php');

//hooks

//scripts and style
add_action('wp_enqueue_scripts', 'theme_styles_and_scripts');
//post type and tax
add_action('init', 'register_post_types');
//themes settings
add_action('after_setup_theme', 'setup_theme');
//remove admin panel
add_filter('show_admin_bar', '__return_false');
//Disable file editor
define('DISALLOW_FILE_EDIT', true);
//Disable xmlrpc
add_filter('xmlrpc_enabled', '__return_false');
// Completely disable editor
add_action('init', 'filin_remove_editor_from_post_type');
// New user's role
add_action( 'after_switch_theme', 'activate_filin_theme' );
add_action( 'switch_theme', 'deactivate_filin_theme' );
