
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body>
<?php
	if ( have_rows('site_brand', 'option') ): the_row(); 
		$background_img = get_sub_field('background_page_ad_img');
		$bg_link = get_sub_field('link');
		$flag = get_sub_field('header_brending');
		if ($background_img !== false && !$flag):
?>
<style type="text/css">
	.upper-main-page-block {
		background:url('<?= $background_img ?>');
	    background-position:top center;
	    background-repeat:no-repeat;
	    background-attachment:fixed;
	    max-width: 1920px;
	    margin-left: auto;
	    margin-right: auto;
	    padding: 0;
	}
	.upper-main-page-info {
	    width: 1920px;
	    padding: 0;
	    height: 283px;
	}
</style>
<div class="upper-main-page-block">
	<a style="height:1700px;" id="adbrandbg" href="<?= $bg_link ?>" target="_blank"></a>
	<div class="upper-main-page-info">
		<!--<img src="" alt="" class="logo-adv">
		<img src="" alt="" class="img-adv">-->
	</div>
<?php elseif($flag): ?>
<style type="text/css">
	.upper-main-page-block {
	    padding: 0;
	}
.upper-main-page-info {
    width: 1920px;
    /*padding: 20px 294px;*/
    padding: 0;
    height: 283px;
   background:url('<?= $background_img ?>');
}
</style>
<div class="upper-main-page-block">
	<a style="height:1700px;" id="adbrandbg" href="<?= $bg_link ?>" target="_blank"></a>
	<div class="upper-main-page-info">
	</div>
<?php endif; ?>
<?php endif; ?>
<div class="main-page-block">
	<header class="header" id="header">
		<div class="container">
			<div class="row">
				<div class="col-4">
					<?php
						if( have_rows('social_networks', 'option') ): ?>
							<ul class="social-list">
								<?php while( have_rows('social_networks', 'option') ) : the_row(); 
                                     if ( get_sub_field('link')['title'] == 'vk'){
                                        $link = get_site_url();
                                    } else if ( get_sub_field('link')['title'] == 'rss') {
                                        $link = '';
                                    } else $link = get_permalink();
                                     ?>
										<li>
											<a target="<?= get_sub_field('link')['target'] ?>" href="<?= get_sub_field('link')['url'] ?><?php echo $link; ?>">
												<svg class="icon">
													<use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#<?= get_sub_field('image') ?>" />
												</svg>
											</a>
										</li>
									<?php
								endwhile; ?>
							</ul>
						<?php
						endif;
					?>
				</div>
				<div class="col-4 col-sm col-header-mob">
					<button class="btn-icon btn-search-mobile btn-search" id="btn-search-mobile" type="button">
						<svg class="icon icon-open">
							<use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#search" />
						</svg>
						<svg class="icon icon-close">
							<use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#close" />
						</svg>
					</button>
					<a href="/" class="logo">
						<img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="Филин">
					</a>
					<button class="btn-icon btn-menu-mobile btn-menu" id="btn-menu" type="button">
						<span class="line"></span>
						<span class="line"></span>
						<span class="line"></span>
					</button>
				</div>
				<div class="col-4 col-sm col-right">
					<?php
						$donate = get_field('donate', 'option');
						if ($donate) :
						?>
							<a target="<?= $donate['target'] ?>" href="<?= $donate['url'] ?>" class="btn-special">
								<span class="line"></span>
								<span class="word row-one">Поддержать</span>
								<span class="word row-two">проект</span>
							</a>
						<?php
						endif;
					?>
				</div>
			</div>
			<?php 
				//Preparing and output menu
				$menu = array(
					'theme_location' => 'top_menu',
					'container'       => 'nav',
					'container_class' => 'nav-menu', 
					'menu_class'      => 'menu',
					'depth'           => 1
				);
				wp_nav_menu($menu);
			?>
			<div class="search-mobile">
				<form class="form-search" method="get" id="form-search" action="<?= esc_url( home_url( '/' ) ) ?>">
					<input type="text" value="<?= get_search_query() ?>" name="s" class="input-form" id="input-search-mob" placeholder="Поиск">
					<button class="btn-icon btn-search" id="btn-search-mob" type="button">
						<svg class="icon">
							<use xlink:href="<?= get_template_directory_uri() ?>/img/icons/svgmap.svg#search" />
						</svg>
					</button>
				</form>
			</div>
		</div>
	</header>